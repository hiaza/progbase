#include <stdio.h>
#include <math.h>
#include <progbase.h>
int main(void)
{
    double x, y, z, a0, a1, a2, a;

    puts("Введіть значення х: ");
    x = getDouble();
    puts("");

    puts("Введіть значення y: ");
    y = getDouble();
    puts("");

    puts("Введіть значення z: ");
    z = getDouble();
    puts("");

    printf("x = %.3lf", x);
    puts("");

    printf("y = %.3lf", y);
    puts("");

    printf("z = %.3lf", z);
    puts("");

    if (((x > 0 || (x == 0 && 1 + y > 0)) && (x > y || (x == y && z > 0)) && pow(x - y,1/z) != 0 && z != 0))
    {   
        a0 = pow(x, y + 1) / pow(x - y, 1 / z);
        printf("a0 = %.3lf", a0);
        puts("");
    }
    else
    {
        puts("a0 Can't be computed");
    }

    if (fabs(x + y) != 0)
    {
        a1 = y / (2 * fabs(x + y));
        printf("a1 = %.3lf", a1);
        puts("");
    }
    else
    {
        puts("a1 Can't be computed");
    }

    if (z != 0)
    {
        a2 = pow((2 + sin(y)), (cos(x) / z + fabs(x - y)));
        printf("a2 = %.3lf", a2);
        puts("");
    }
    else
    {
        puts("a2 Can't be computed");
    }

    if (((x > 0 || (x == 0 && 1 + y > 0)) && (x > y || (x == y && z > 0)) && pow(x - y,1/z) != 0) && fabs(x + y) != 0 && z != 0)
    {
        a = a1 + a2 + a0;
        printf("a = %.3lf", a);
        puts("");
    }
    else
    {
        puts("a Can't be computed");
    }

    puts("---------");
    return 0;
}