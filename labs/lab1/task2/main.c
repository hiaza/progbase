#include <stdio.h>
#include <progbase.h>
#include <math.h>
#include <stdbool.h>
int main(void)
{
    int a, b, c, modmin, sum2, max, min;
    puts("Введіть a");
    a = getInt();
    puts("");
    puts("Введіть b");
    b = getInt();
    puts("");
    puts("Введіть C");
    c = getInt();
    puts("");
    bool result;

    if (a < 0 && b < 0 && c < 0)
    {
        if (a <= b && a <= c)
        {
            modmin = abs(a);
            sum2 = b + c;
        }

        else
        {
            if (b <= c)
            {
                modmin = abs(b);
                sum2 = a + c;
            }
            else
            {
                modmin = abs(c);
                sum2 = a + b;
            }
        }

        printf("a=%i\n", a);
        printf("b=%i\n", b);
        printf("c=%i\n", c);
        puts("All negative");
        puts("modmin:");
        printInt(modmin);
        puts("");
        puts("sum2:");
        printInt(sum2);
        puts("");
        if (modmin > 8 || abs(sum2) > 16)
            result = true;
        else
            result = false;
    }
    else
    {
        if (a >= 0 && b >= 0 && c >= 0)
        {
            if (a <= b && a <= c)
            {
                min = a;
            }
            else
            {
                if (b <= c)
                    min = b;
                else
                    min = c;
            }
            if (a >= b && a >= c)
            {
                max = a;
            }
            else
            {
                if (b >= c)
                    max = b;
                else
                    max = c;
            }
            printf("a=%i\n", a);
            printf("b=%i\n", b);
            printf("c=%i\n", c);
            puts("All positive");
            puts("max:");
            printInt(max);
            puts("");
            puts("min:");
            printInt(min);
            puts("");
            if (max - min > 32)
                result = true;
            else
                result = false;
            puts("");
        }
        else
        {
            if ((a < 0 && b < 0) || (a < 0 && c < 0) || (b < 0 && c < 0))
            {
                puts("Two negative");
                if (a < 0 && b < 0)
                {
                    printf("a=%i\n", a);
                    printf("b=%i\n", b);
                    if ((a + b) * 5 > -256)
                        result = true;
                    else
                        result = false;
                    puts("");
                }
                else
                {
                    if (a < 0 && c < 0)
                    {
                        printf("a=%i\n", a);
                        printf("c=%i\n", c);
                        if ((a + c) * 5 > -256)
                            result = true;
                        else
                            result = false;
                        puts("");
                    }
                    else
                    {
                        printf("b=%i\n", b);
                        printf("c=%i\n", c);
                        if ((b + c) * 5 > -256)
                            result = true;
                        else
                            result = false;
                        puts("");
                    }
                }
            }
            else
            {
                puts("One negative");
                if (a < 0)
                {
                    printf("a=%i\n", a);
                    if (a % 2 == 0)
                        result = true;
                    else
                        result = false;
                    puts("");
                }
                else
                {
                    if (b < 0)
                    {
                        printf("b=%i\n", b);
                        if (b % 2 == 0)
                            result = true;
                        else
                            result = false;
                        puts("");
                    }
                    else
                    {
                        printf("c=%i\n", c);
                        if (c % 2 == 0)
                            result = true;
                        else
                            result = false;
                        puts("");
                    }
                }
            }
        }
    }
    printf("Result: %s\n",result ? "true" : "false");
    puts("");
    return 0;
}