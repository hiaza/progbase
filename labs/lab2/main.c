#include <stdlib.h>
#include <stdio.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <time.h>
#include <progbase.h>
#include <ctype.h>
#include <string.h>
void help1();
void help2();
void help3();
void chistka();
void chistka2();
void chistka3();
void chistka4();
void random1(int lenght, int*arr);
void random2(int lenght, int arr[lenght][lenght]);
void fillStr(int lenght, char*str);
void obnul1(int lenght, int*arr);
void obnul2(int lenght, int arr[lenght][lenght]);
void cleanStr(int lenght, char*str);
void min1(int lenght, int*arr);
void max2(int lenght, int arr[lenght][lenght]);
void copyofRangeStr(int lenght, char*str);
void sum1(int lenght, int*arr);
void sum2(int lenght, int arr[lenght][lenght]);
void devideStr(int lenght, char*str);
void dobutok1 (int lenght, int*arr);
void sumElements2(int lenght, int arr[lenght][lenght]);
void theShortest(int lenght, char*str);
void changing1 (int lenght, int*arr);
void changing2 (int lenght, int arr[lenght][lenght]);
void numberPoint(int lenght, char*str);
void increasing1 (int lenght, int*arr);
void Znumbers(int lenght, char*str);
void changeElement (int lenght, int arr[lenght][lenght]);
void cleanBuffer();
int main(void)
{
    srand(time(0));
    char mainManu[][45]=
    {
        "1)Одномірний масив",
        "2)Двовимірний масив",
        "3)Обробка рядків мови С",
        "4)Вихід з програми."
    };
    Console_hideCursor();
    char n;
    char k;
    int lenght;
    int width;
    int len;
    do{
        Console_clear();
        for (int i=0; i<4;i++)
        {
            puts(mainManu[i]);
        };
       // printf("Введіть номер завдання:\n");
        n = Console_getChar();
        switch (n){

            case 49:{
            Console_clear();
            puts("Введіть довжину масиву");
            scanf("%i",&lenght);
            if (lenght<=0){
                do{
                    Console_clear();
                    puts("Помилка вводу");
                    puts("Введіть довжину масиву");
                    cleanBuffer();
                    scanf("%i",&lenght);
                }while(lenght<=0);
            }
            int array[lenght];
            for(int i = 0; i < lenght;i++){
            array[i]=0;
            }
            Console_clear();
            Console_reset();
            do
            {
                Console_setCursorPosition(1,0);
                puts("                                  ЗОНА ВВОДУ     ");
                Console_setCursorPosition(13,0);
                puts("                                  ЗОНА ВИВОДУ     ");
                chistka2();
                printf("Довжина масиву %i\n",lenght);
                printf("Введіть номер дії з масивом.\nДля довідки натисніть 0.\n");
                Console_reset();
                chistka3();
                puts("Масив:");
                puts(" "); 
                for(int i = 0; i < lenght;i++)
                {
                    printf("%5i",array[i]);
                }
                Console_reset();
                k = Console_getChar();
                switch (k){
                    case 49:
                    {
                        random1(lenght,array);
                        break;
                    }
                    case 50:
                    {
                        obnul1(lenght, array);
                        break;
                    }
                    case 51:
                    {
                        min1(lenght, array);
                        break;
 
                    }
                    break;
                    case 52:
                    {
                        sum1(lenght, array);
                        break; 
                    }
                    case 53:
                    {
                        dobutok1(lenght, array);
                        break; 
                    }
                    case 54:
                    {
                        changing1(lenght, array);
                        break;
                    }
                    case 55:
                    {
                        increasing1(lenght, array);
                        break;
                    }
                    case 48:
                    help1();
                    break;
                    case 45:
                    chistka();
                    break;
                    case 127:
                    break;
                    default:
                    break;
                }
            } while(k!=127); 
            break;
        }
            case 50:
            {
                Console_clear();
                puts("Введіть розмір матриці");
                scanf("%i",&width);
                if (width<=0){
                    do{
                        Console_clear();
                        puts("Помилка вводу");
                        puts("Введіть розмір матриці");
                        cleanBuffer();
                        scanf("%i",&width);
                    }while(width<=0);
                }
                Console_clear();
                int array2[width][width];
                for(int i = 0; i < width;i++){
                    for(int j = 0; j < width;j++)
                    {
                        array2[i][j]=0;
                    }
                }
                Console_clear();
                Console_reset();
                do
                {
                    Console_setCursorPosition(1,0);
                    puts("                                  ЗОНА ВВОДУ     ");
                    Console_setCursorPosition(13,0);
                    puts("                                  ЗОНА ВИВОДУ     ");
                    chistka2();
                    printf("Розмір матриці - %i.\n",width);
                    printf("Введіть номер дії з матрицею.\nДля довідки натисніть 0.");
                    Console_reset();
                    chistka4();
                    puts("Масив:");
                    puts(" "); 
                    for(int i = 0; i < width;i++){
                        for(int j = 0; j < width;j++)
                        {
                            printf("%5i",array2[i][j]);
                        }
                        puts(" ");
                    }
                    Console_reset();
                    k = Console_getChar();
                    switch (k){
                        case 49:
                        {
                            random2(width, array2);
                            break;
                        }
                        case 50:
                        {
                            obnul2(width,array2);
                            break;
                        }
                        case 51:
                        {
                            max2(width,array2);
                            break;
                        }
                        case 52:    
                        {
                            sum2(width,array2);
                            break;    
                        }
                        case 53:
                        {
                            sumElements2(width,array2);
                            break;
                        }
                        case 54:
                        {
                            changing2(width,array2);
                            break;
                        }
                        case 55:
                        {
                            changeElement(width,array2);
                            break;
                        }
                        case 48:
                        help2();
                        break;
                        case 45:
                        chistka();
                        break;
                        case 127:
                        break;
                        default:
                        break;
                    }
                } while(k!=127);
                
                break;
            }
         
            case 51:
            {
                Console_clear();
                puts("Введіть розмір строки:");
                scanf("%i",&len);
                if (len<=0){
                    do{
                        Console_clear();
                        puts("Помилка вводу");
                        puts("Введіть розмір строки");
                        scanf("%i",&len);
                    }while(len<=0);
                }
                cleanBuffer();
                Console_clear();
                char str[len+2];
                str[0] = 0;
                str[len] ='\0';
                for(int i = 0; i < len;i++){
                    str[i] = (rand()%(92)+33);
                }
                Console_clear();
                Console_reset();
                do
                {
                    Console_setCursorPosition(1,0);
                    puts("                                  ЗОНА ВВОДУ     ");
                    Console_setCursorPosition(13,0);
                    puts("                                  ЗОНА ВИВОДУ     ");
                    chistka2();
                    printf("Введіть номер дії з строкою.\nДля довідки натисніть 0.");
                    chistka3();
                    printf("Поточна строка: %s\n",str); 
                    int l = strlen(str);
                    printf("Розмір строки - %i.", l);
                    Console_reset();                 
                    k = Console_getChar();
                    switch (k){
                        case 49:
                        {
                            fillStr(len+2,str);
                            break;
                        }
                        case 50:
                        {
                            cleanStr(len,str);
                            break;
                        }
                        case 51:
                        {
                            copyofRangeStr(len,str);
                            break;
                        }
                        case 52:    
                        {
                            devideStr(len,str);
                            break; 
                        }   
                        case 53:
                        {
                            theShortest(len,str);
                            break;
                        }     
                        case 54:
                        {
                            numberPoint(len,str);
                            break;
                        }
                        case 55:
                        {
                            Znumbers(len,str);
                            break;
                        } 
                        case 48:
                        help3();
                        break;
                        case 45:
                        chistka();
                        break;
                        case 127:
                        break;
                        default:
                        break;
                    }
                } while(k!=127);
                
                break;
            }

            case 52:
            {
                break;
            }

            default:{
                puts("Неправильний номер завдання");
                sleepMillis(1000);
                break;
            }
        }
    }while(n!=52);
    Console_showCursor();
    return 0;
}

void increasing1 (int lenght, int*arr){
    chistka2();
    int n;
    puts("Введіть число на яке бажаєте збільшити масив:");
    scanf("%i",&n);
    Console_reset();
    chistka3();    
    puts("Масив до:");
    puts(" "); 
    for(int i = 0; i<lenght; i++)
    {
        printf("%5i",arr[i]);         
    }
    puts(" "); 
    puts("Масив після:");
    puts(" "); 
    for(int i = 0; i<lenght; i++)
    {
        arr[i] = arr[i] + n;
        printf("%5i",arr[i]);
    }
    puts(" "); 
    puts(" "); 
    Console_getChar();
    Console_reset();
    Console_clear();
}

void changing2 (int lenght, int arr[lenght][lenght]){
    chistka2();
    Console_reset();
    chistka4();
    int min = arr[0][0];
    int iMinIndex = 0;
    int jMinIndex = 0;
    int max = arr[0][0];
    int iMaxIndex = 0;
    int jMaxIndex = 0;
    for(int i = 0; i < lenght; i++){
       for(int j = 0; j < lenght; j++){
        if (arr[i][j]<min){
            min = arr[i][j];
            iMinIndex = i;
            jMinIndex = j;
        }
        if(arr[i][j]>max){
            max = arr[i][j];
            iMaxIndex = i;
            jMaxIndex = j;
        }
       }
    }
    puts("Масив до:");
    puts(" "); 
    for(int i = 0; i<lenght; i++)
    {
        for(int j = 0; j < lenght; j++)
        {
            if(i == iMinIndex && j == jMinIndex)
            { 
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i][j]);
                Console_reset();
            }
            else 
            {
                 if(i == iMaxIndex && j == jMaxIndex)
                 { 
                    Console_setCursorAttribute(BG_RED);
                    printf("%5i",arr[i][j]);
                    Console_reset();
                 }
                 else
                 {
                    printf("%5i",arr[i][j]);
                 }
            }
        }   
        puts(" ");         
    }
    puts(" "); 
    puts(" "); 
    puts("Масив після:");
    for(int i = 0; i<lenght; i++)
    {
        for(int j = 0; j < lenght; j++)
        {
            if(i == iMinIndex && j == jMinIndex)
            {
                arr[i][j] = max; 
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i][j]);
                Console_reset();
            }
            else 
            {
                 if(i == iMaxIndex && j == jMaxIndex)
                 { 
                    arr[i][j] = min;
                    Console_setCursorAttribute(BG_RED);
                    printf("%5i",arr[i][j]);
                    Console_reset();
                 }
                 else
                 {
                    printf("%5i",arr[i][j]);
                 }
            }
        }
        puts(" ");          
    }
    puts(" "); 
    puts(" "); 
    Console_getChar();
    Console_reset();
    Console_clear();
}

void changing1 (int lenght, int*arr){
    chistka2();
    Console_reset();
    chistka3();
    int min = arr[0];
    int minIndex = 0;
    int max = arr[0];
    int maxIndex = 0;
    for(int i = 0; i< lenght; i++){
        if (arr[i]<min){
            min = arr[i];
            minIndex = i;
        }
        if(arr[i]>max){
            max = arr[i];
            maxIndex = i;
        }
    }
    puts("Масив до:");
    puts(" "); 
    for(int i = 0; i<lenght; i++)
    {
        if(i == minIndex)
        { 
            Console_setCursorAttribute(BG_RED);
            printf("%5i",arr[i]);
            Console_reset();
        }
         else 
         {
             if(i == maxIndex)
             { 
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i]);
                Console_reset();
             }
             else
             {
                printf("%5i",arr[i]);
             }
         }
            
    }
    puts(" "); 
    puts("Масив після:");
    puts(" "); 
    for(int i = 0; i<lenght; i++)
    {
        if(i == minIndex)
        {
            arr[i] = max;
            Console_setCursorAttribute(BG_RED);
            printf("%5i",arr[i]);
            Console_reset();
        }
         else 
         {
             if(i == maxIndex)
             { 
                arr[i] = min;
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i]);
                Console_reset();
             }
             else
             {
                printf("%5i",arr[i]);
             }
         }
            
    }
    puts(" "); 
    Console_getChar();
    Console_reset();
    Console_clear();
}

void sum2(int lenght, int arr[lenght][lenght]){
    chistka2();
    Console_reset();
    chistka4();
    int sum = 0;  
    puts("Масив:");  
    puts(" ");    
    for(int i = 0;i<lenght;i++)
    {
        for( int j = 0; j<lenght;j++)
        {
            if(i + j == lenght - 1)
            {
                sum = sum + arr[i][j];
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i][j]);
                Console_reset();
            }
            else
            {
                printf("%5i",arr[i][j]);
            }
        }
        puts(" ");   
    }
    puts(" ");
    printf("Сума елементів побічної діагоналі - %i",sum);
    Console_getChar();
    Console_reset();
    Console_clear();
}

void sum1(int lenght, int*arr){
    chistka2();
    Console_reset();
    chistka3();
    int sum=0;
    puts("Масив:");  
    puts(" ");  
    for(int i = 0; i<lenght; i++){
        sum = sum + arr[i];
        printf("%5i",arr[i]);
    }
    puts(" "); 
    puts(" "); 
    printf("Sum = %i\n",sum);
    Console_getChar();
    Console_reset();
    Console_clear();
}

void sumElements2(int lenght, int arr[lenght][lenght]){
    chistka2();
    int ind;
    puts("Введіть індекс стовпця");
    scanf("%i",&ind);
    while (ind<0 || ind>lenght-1){
        chistka2();
        puts("Неправильний індекс!");
        puts("Введіть ще раз індекс стовпця");
        scanf("%i",&ind);
    }
    Console_reset();
    chistka4();
    int sum = 0;  
    puts("Масив:");  
    puts(" ");     
    for(int i = 0;i<lenght;i++)
    {
        for( int j = 0; j<lenght;j++)
        {
            if(j == ind)
            {
                sum = sum + arr[i][j];
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i][j]);
                Console_reset();
            }
            else
            {
                printf("%5i",arr[i][j]);
            }
        }
        puts(" ");   
    }
    puts(" ");
    printf("Сума елементів стовпця з індексом %i - %i",ind,sum);
    Console_getChar();
    Console_reset();
    Console_clear();
}

void changeElement(int lenght, int arr[lenght][lenght]){
    chistka2();
    int iInd;
    int jInd;
    int num;
    puts("Введіть індекс рядка(і)");
    scanf("%i",&iInd);
    while (iInd<0 || iInd>lenght-1){
        chistka2();
        puts("Неправильний індекс!");
        puts("Введіть ще раз індекс рядка(і)");
        scanf("%i",&iInd);
    }
    puts("Введіть індекс стовпця(j)");
    scanf("%i",&jInd);
    while (jInd<0 || jInd>lenght-1){
        chistka2();
        puts("Неправильний індекс!");
        puts("Введіть ще раз індекс стовпця(j)");
        scanf("%i",&jInd);
    }
    chistka2();
    puts("Введіть цілочисельне значення, яке потрібно присвоїти:");
    scanf("%i",&num);
    chistka2();
    printf("Індекс елемента (%i,%i)",iInd,jInd);
    puts(" ");
    printf("Значення, що потрібно присвоїти - %i",num);
    puts(" ");
    Console_reset();
    chistka4();
    puts("Масив до присвоєння:");    
    puts(" ");   
    for(int i = 0;i<lenght;i++)
    {
        for( int j = 0; j<lenght;j++)
        {
            if(j == jInd && i == iInd)
            {
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i][j]);
                Console_reset();
            }
            else
            {
                printf("%5i",arr[i][j]);
            }
        }
        puts(" ");   
    }
    puts(" ");
    puts("Масив після присвоєння:");    
    puts(" ");   
    for(int i = 0;i<lenght;i++)
    {
        for( int j = 0; j<lenght;j++)
        {
            if(j == jInd && i == iInd)
            {
                arr[i][j] = num;
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i][j]);
                Console_reset();
            }
            else
            {
                printf("%5i",arr[i][j]);
            }
        }
        puts(" ");   
    }
    puts(" ");
    Console_getChar();
    Console_reset();
    Console_clear();
}

void dobutok1 (int lenght, int*arr){
    chistka2();
    Console_reset();
    chistka3();
    long mult = 1;
    int k = 0;
    puts("Масив:");  
    puts(" ");   
    for(int i = 0; i<lenght; i++){
        if(arr[i]<0){
            mult = mult * arr[i];
            k = k + 1;
        }
        printf("%5i",arr[i]);
    }
    puts(" "); 
    puts(" "); 
    puts("Від'ємні елементи масиву:");  
    puts(" ");
    for(int i = 0; i<lenght; i++){
        if(arr[i]<0){
            printf("%5i",arr[i]); 
        }
    }
    puts(" "); 
    puts(" ");
    if (k!=0){
        printf("Добуток від'ємних чисел масиву = %ld\n",mult);
    }
    else{
        puts("Немає від'ємних елементів");
    }
    Console_getChar();
    Console_reset();
    Console_clear();
}

void random2(int lenght, int arr[lenght][lenght]){
    chistka2();
    int L,H;
    puts("Введіть L, L - нижня границя");
    scanf("%i",&L);
    puts("Введіть H, H - верхня границя");
    scanf("%i",&H);
    if(L>H){
        int temp = H;
        H = L;
        L = temp; 
    }
    Console_reset();
    chistka4();
   // printf("L = %i\nH = %i\n",L,H);
   //  puts(" ");  
    puts("Масив:");    
    puts(" ");   
    for(int i = 0;i<lenght;i++){
        for(int j = 0; j < lenght; j++)
        {  
            arr[i][j]=rand() % (H-L+1) + L;
            printf("%5i",arr[i][j]);
        }
        puts(" ");
    }
    puts(" ");
    Console_getChar();
    Console_reset();
    Console_clear();
}


void random1(int lenght, int arr[lenght]){
    chistka2();
    int L,H;
    puts("Введіть L, L - нижня границя");
    scanf("%i",&L);
    puts("Введіть H, H - верхня границя");
    scanf("%i",&H);
    if(L>H){
        int temp = H;
        H = L;
        L = temp; 
    }
    Console_reset();
    chistka3();
   // printf("L = %i\nH = %i\n",L,H);
   //  puts(" ");  
    puts("Масив:");    
    puts(" ");   
    for(int i = 0;i<lenght;i++){
        arr[i]=rand() % (H-L+1) + L;
        printf("%5i",arr[i]);
    }
    puts(" ");
    Console_getChar();
    Console_reset();
    Console_clear();
}

void max2(int lenght, int arr[lenght][lenght]){
    chistka2();
    Console_reset();
    chistka4(); 
    puts("Масив:");    
    puts(" ");   
    int max = arr[0][0];
    for(int i = 0;i<lenght;i++){
        for(int j = 0; j < lenght; j++)
        {
            if(arr[i][j]>max)
            {
                max = arr[i][j];
            }
        }
    }
    for(int i = 0;i<lenght;i++){
        for( int j = 0; j < lenght; j++)
        {
            if(arr[i][j]==max)
            {
                Console_setCursorAttribute(BG_RED);
                printf("%5i",arr[i][j]);
                Console_reset();        
            }
            else
            {
                printf("%5i",arr[i][j]);
            }
        }
        puts(" ");
    }
    puts(" ");
    puts(" ");
    printf("Max = %i\n", max);
    printf("Index = ");
    for(int i = 0;i<lenght;i++){
        for( int j = 0; j < lenght; j++)
        {
            if(arr[i][j]==max)
            {
                printf("(%i,%i), ",i,j);
            }
        } 
    }
    puts(" ");
    Console_getChar();
    Console_reset();
    Console_clear();
}

void min1(int lenght, int arr[lenght]){
    chistka2();
    Console_reset();
    chistka3();
   // printf("L = %i\nH = %i\n",L,H);
   // puts(" ");  
    puts("Масив:");    
    puts(" ");   
    int min = arr[0];
    for(int i = 0;i<lenght;i++){
        if(arr[i]<min){
            min = arr[i];
        }
    }
    for(int i = 0;i<lenght;i++){
        if(arr[i]==min)
        {
            Console_setCursorAttribute(BG_RED);
            printf("%5i",arr[i]);
            Console_reset();        
        }
        else
        {
            printf("%5i",arr[i]);
        }
    }
    puts(" ");
    puts(" ");
    printf("Min = %i\n", min);
    printf("Index = ");
    for(int i = 0;i<lenght;i++){
        if(arr[i]==min)
        {
            printf("%i, ",i);
        }
    }
    puts(" ");
    Console_getChar();
    Console_reset();
    Console_clear();
}
void fillStr(int lenght, char*str){
    
    chistka2();
    puts("Введіть строку:");
    fgets(str,lenght,stdin);
    if (strlen(str) > lenght-1)
    {
        cleanBuffer();
    }
    str[strlen(str)-1] = '\0';
    Console_reset();
    chistka3();
    printf("Поточна строка: %s",str);
    Console_getChar();
    Console_reset();
    Console_clear();
}
void cleanStr(int lenght, char*str){
    chistka2();
    puts("Строка очищена. Натисніть будь-яку клавішу для продовження...");
    str[0] = '\0';
    Console_reset();
    chistka3();
    Console_getChar();
    Console_reset();
    Console_clear();
}
void copyofRangeStr(int lenght, char*str){
    chistka2();
    if (strlen(str)!=0)
    {
        int ind;
        int dov;
        puts("Введіть позицію початку підстроки:");
        scanf("%i",&ind);
        if(ind<0 || ind>=strlen(str))
        {
            do
            {
                chistka2();
                puts("Невірна позиція");
                puts("Введіть позицію початку підстроки:");
                scanf("%i",&ind);
        
            }while(ind<0 || ind>=strlen(str));
        }
        cleanBuffer();
        chistka2();
        puts("Введіть довжину підстроки:");
        scanf("%i",&dov);
        if (dov<=0 || dov+ind>strlen(str))
        {
            do
            {
                chistka2();
                puts("Невірна довжина");
                puts("Введіть довжину підстроки:");
                scanf("%i",&dov);
            }while(dov<0 || dov+ind>strlen(str));
        }
        cleanBuffer();
        Console_reset();
        chistka3();
        printf("Поточна строка: %s\n",str);
        char buffer[1000];
        buffer[0]='\0';
    
        int n = ind;
        for(int i = 0; i < dov; i++,n++)
        {
            buffer[i] = str[n];
        }
        buffer[dov] = '\0'; 
        puts(buffer);
    }
    else
    {
        puts("Строка пуста");
    }
    Console_getChar();
    Console_reset();
    Console_clear();
}

void devideStr(int lenght, char*str)
{
    chistka2();
    if (strlen(str)!=0)
    {
        char ind;
        puts("Введіть символ:");
        ind = getChar();
        if (strchr(str,ind) == 0)
        {
            do
            {
                chistka2();
                puts("В строці не знайдено данного символу");
                puts("Введіть iнший символ:");
                ind = getChar();
            }while(strchr(str,ind) == 0);
        }
        Console_reset();
        chistka3();
        printf("Поточна строка: %s\n",str);
        puts("Отримані підстроки:");
        for(int i = 0; i < strlen(str); i++)
        {
            if(ind == str[i])
            {
                puts(" ");
            }
            else
            {
                printf("%c",str[i]);
            }
        }
    }
    else
    {
        puts("Строка пуста");
    }
    Console_getChar();
    Console_reset();
    Console_clear();
}
void theShortest(int lenght, char*str)
{
    chistka2();
    if (strlen(str)!=0)
    {
        Console_reset();
        chistka3();
        char buffer[1000];
        buffer[0]='\0';
        char g[1000];
        g[0] = 0;
        int counter = 0;
        int min = 10000;
        printf("Поточна строка: %s\n",str);
        puts("Найкоротше слово:");
        for(int i = 0; i < strlen(str); i++)
        {
            if (isalpha(str[i]))
            {
                buffer[counter] = str[i];
                counter++;
            }
            else
            {
                if (min > counter && counter!=0)
                {
                    min = counter;
                    g[0]='\0';
                    buffer[min] = '\0';
                    counter = 0;
                    strcpy(g, buffer);
                    g[min] = '\0';
                }
                else
                {
                    counter = 0;
                }
            }
        }
        if (min == 10000){
            puts(str);
        }
        else{
            puts(g);
        }
    
        
    }
    else
    {
        puts("Строка пуста");
    }
    Console_getChar();
    Console_reset();
    Console_clear();
}
void numberPoint(int lenght, char*str)
{
    chistka2();
    if (strlen(str)!=0)
    {
        Console_reset();
        chistka3();
        char buffer[1000];
        buffer[0] = 0;
        char g[1000];
        g[0] = '\0';
        int counter = 0;
        printf("Поточна строка: %s\n",str);
        puts("Дробові числа:");
        for(int i = 0; i < strlen(str); i++)
        {
            if (isdigit(str[i]))
            {
                g[counter] = str[i];
                counter++;
            }
            else
            {
                if (counter > 0 && str[i]=='.' && isdigit(str[i+1]))
                {
                    g[counter] = '.';
                    do{
                        i = i+1;
                        g[counter + 1] = str[i];
                        counter = counter + 1;
                    }while(isdigit(str[i+1]) && i < strlen(str));
                    g[counter + 1] = ' ';
                    g[counter + 2] = '\0';
                    strcat(buffer, g);
                    counter = 0;
                }
                else
                {
                    counter = 0;
                }
            }
        }
        puts(buffer);        
    }
    else
    {
        puts("Строка пуста");
    }
    Console_getChar();
    Console_reset();
    Console_clear();
}
void Znumbers(int lenght, char*str)
{
    chistka2();
    if (strlen(str)!=0)
    {
        Console_reset();
        chistka3();
        int mult = 1;
        int counter = 0;
        char buffer[] = {"1234567890"};
        for(int i = 0;i<strlen(str);i++)
        {
            if (strchr(buffer,str[i])){
                counter = counter + 1;
                break;
            }  
        }
        printf("Поточна строка: %s\n",str);
        for(int i = 0; i < strlen(str); i++)
        {
            if (str[i]=='-' && isdigit(str[i+1]))
            {
                mult = mult * (-1) *(str[i+1] - 48);
                i = i + 1;
            }
            else
            {
                if(isdigit(str[i]))
                {
                    mult = mult * (str[i]-48);
                }
            }
            
        }
        if (counter == 0){
        printf("Немає цілих чисел.");
        }        
        else
        printf("Добуток цілих чисел: %i",mult);
    }
    else
    {
        puts("Строка пуста");
    }
    Console_getChar();
    Console_reset();
    Console_clear();
}
void obnul2(int lenght, int arr[lenght][lenght]){
    chistka2();
    Console_reset();
    chistka4();  
    puts("Масив:");    
    puts(" ");   
    for(int i = 0;i<lenght;i++)
    {
        for( int j = 0; j<lenght;j++)
        {
            arr[i][j] = 0 ;
            printf("%5i",arr[i][j]);
        }
        puts(" ");   
    }
    puts(" ");
    Console_getChar();
    Console_reset();
    Console_clear();
}

void obnul1(int lenght, int arr[lenght]){
    chistka2();
    Console_reset();
    chistka3();
   // printf("L = %i\nH = %i\n",L,H);
   //  puts(" ");  
    puts("Масив:");    
    puts(" ");   
    for(int i = 0;i<lenght;i++){
        arr[i] = 0 ;
        printf("%5i",arr[i]);
    }
    puts(" ");
    Console_getChar();
    Console_reset();
    Console_clear();
}


void chistka(){
    char chistka[]={"                                                                                         "};
    for (int i=0; i<10;i++){
        Console_setCursorPosition(2+i,100);
        puts(chistka);
    } 
}
void chistka2(){
    Console_reset();
    char chistka[]={"                                                                                 "};
    Console_setCursorPosition(2,0);
    puts("---------------------------------------------------------------------------------");
    for (int i=0; i<9;i++){
        Console_setCursorAttribute(BG_WHITE);
        Console_setCursorPosition(3+i,0);
        puts(chistka);
    } 
    Console_setCursorAttribute(BG_DEFAULT);
    puts("---------------------------------------------------------------------------------");
    Console_setCursorPosition(3,0);
    Console_setCursorAttribute(BG_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    //Console_reset();
}

void chistka3(){
    Console_reset();
    char chistka[]={"                                                                                                                                                                                    "};
    Console_setCursorPosition(14,0);
    puts("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    for (int i=0; i<9;i++){
        Console_setCursorPosition(15+i,0);
        puts(chistka);
    } 
    puts("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");  
    Console_setCursorPosition(15,0);
}

void chistka4(){
    Console_reset();
    char chistka[]={"                                                                                                                                                                                    "};
    Console_setCursorPosition(14,0);
    puts("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");  
    for (int i=0; i<18;i++){
        Console_setCursorPosition(15+i,0);
        puts(chistka);
    } 
    Console_setCursorPosition(15,0);
}

void help1(){
    chistka();
    char help1[][200]={
        "    Доступні операції над масивом:",
            "1)Заповнити масив випадковими числами від L до H.",
            "2)Обнулити всі елементи масиву.",
            "3)Знайти мінімальний елемент масиву та його індекс.",
            "4)Знайти суму елементів масиву.",
            "5)Вивести добуток від'ємних елементів масиву.",
            "6)Поміняти місцями значення максимального і мінімального елементів масиву.",
            "7)Збільшити всі елементи масиву на введене число.",
            "(-) Закрити довідку.",
            "(Backspace) повернутися до вибору завдання"
        };
        
    for (int i=0; i<10;i++){
        Console_setCursorPosition(2+i,100);
        puts(help1[i]);

    }
    Console_reset();
}
void help2(){
    chistka();
    char help2[][200]={
        "    Доступні операції над матрицею:",
            "1)Заповнити масив випадковими числами від L до H.",
            "2)Обнулити всі елементи масиву.",
            "3)Знайти максимальний елемент та його індекси (i та j).",
            "4)Знайти суму елементів побічної діагоналі масиву.",
            "5)Знайти суму елементів стовпця за заданим індексом.",
            "6)Поміняти місцями максимальний і мінімальний елементи масиву.",
            "7)Змінити значення елементу за вказаними індексами на задане.",
            "(-) Закрити довідку.",
            "(Backspace) повернутися до вибору завдання"
        };
        
    for (int i=0; i<10;i++){
        Console_setCursorPosition(2+i,100);
        puts(help2[i]);

    }
    Console_reset();
}
void help3(){
    chistka();
    char help3[][200]={
        "    Доступні операції над рядком:",
            "1)Заповнити строку введеним значенням із консолі.",
            "2)Очистити строку.",
            "3)Вивести підстроку із заданої позиції і заданої довжини.",
            "4)Вивести список підстрок, розділених заданим символом.",
            "5)Вивести найкоротше слово (слова - непуста послідовність буквенних символів).",
            "6)Знайти та вивести всі дробові числа, що містяться у строці.",
            "7)Знайти та вивести добуток всіх цілих чисел, що містяться у строці.",
            "(-) Закрити довідку.",
            "(Backspace) повернутися до вибору завдання"
        };
        
    for (int i=0; i<10;i++){
        Console_setCursorPosition(2+i,100);
        puts(help3[i]);

    }
    Console_reset();
}
void cleanBuffer() {
    char c = 0;
    while(c != '\n') {
        c = getchar();
    }
}