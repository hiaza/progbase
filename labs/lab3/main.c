#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <progbase/console.h>
#include <progbase.h>
#include <assert.h>
enum MainCommands
{
    NO,
    UP,
    DOWN,
    ENTER,
    BACK
};
const int LENGTH = 10;
enum MainCommands getMainCommandsUserInput();
void printMainMenu(char vc[][300], int ex);
void cleanBuffer();
struct WritersData
{
    char theGreatestPoem[100];
    int yearOfBirth;
    float rating;
};

struct Writer
{
    char name[100];
    struct WritersData data;
};


int writerDataToStr(struct WritersData *p, char *buffer, int bufferLength);

bool isEqual(struct Writer a,struct Writer b);

bool isEqualData(struct WritersData a,struct WritersData b);

bool isEqualWriters(struct Writer*first,struct Writer*second);

void test();

int writerToStr(struct Writer *p, char *buffer, int bufferLength);

int strToWriterData(const char *str, struct WritersData *p);

int strToWriter(const char *str, struct Writer *p);

struct Writer *newWriter(const char *name, const char *poem, int year, float rating);

void freeWriter(struct Writer *writer);

void clearArray(int len, struct Writer *array[]);

int countWriters(int len, struct Writer *array[]);

bool addWriter(int len, struct Writer *array[], struct Writer *newStudent);

struct Writer *removeWritersAt(int len, struct Writer *array[], int index);

struct Writer *rewriteWriter(struct Writer **array, int index, char *str);

struct Writer *rewriteSomething(struct Writer **array, int index, int pos);

void printwriterdata(struct Writer **p, int index);

void printWriter(struct Writer **p, int index);

void printChosedWriter(int len, struct Writer **p, int index);

void printChosedField(int len, struct Writer **p, int index, int ind);

void printwriters(int len, struct Writer *arr[]);

struct Writer *findWriters(int len, struct Writer **array, int year, struct Writer **yearWr);

int fileExists(const char *fileName);

long getFileSize(const char *fileName);

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);

void ToDo(char menu2[][300], struct Writer **writers);

void saveToFile(char*text, const char*fileName){
    FILE*file = fopen(fileName,"w");
    fputs(text,file);
    fclose(file);
}

#define __LEN(A) sizeof(A) / sizeof(A[0])

char*const ALLOWED_COMMANDS [] = {
    "-test"
};

int getCommandIndex(const char * command){
    for ( int i = 0; i < __LEN(ALLOWED_COMMANDS);i++){
        char * allowedCommand = ALLOWED_COMMANDS[i];
        if ( strcmp( command, allowedCommand) == 0){
            return i;
        }
    }
    return -1;
}

int main(int argc, char * argv[])
{
    Console_hideCursor();
    if(argc == 1){

    Console_clear();
    puts(" ");
    puts("Welcome to our database of writers");
    sleepMillis(2000);
    Console_clear();

    char menu[][300] = {
        "Створити новий пустий список сутностей.",
        "Зчитати список сутностей із текстового файлу.",
    };
    char menu2[][300] = {
        "Додати у кінець списку нову сутність із вказаними користувачем даними",
        "Видалити сутність із вказаної позиції у списку",
        "Перезаписати всі дані полів сутності у вказаній позиції",
        "Перезаписати обране поле даних сутності із вказаної позиції у списку",
        "Знайти всіх письменників, що народились до року Х",
        "Зберегти поточний список сутностей на файлову систему",
    };
    int Index = 0;

    printMainMenu(menu, Index);

    while (1)
    {
        Console_clear();
        printMainMenu(menu, Index);
        Console_setCursorPosition(3, 0);
        puts("  ");
        enum MainCommands commands = getMainCommandsUserInput();
        switch (commands)
        {
        case UP:
            Index -= 1;
            if (Index < 0)
            {
                Index = 2 - 1;
            }
            break;
        case DOWN:
            Index = (Index + 1) % 2;
            break;
        case ENTER:
            if (Index == 0)
            {
                struct Writer *writers[LENGTH];
                for (int i = 0; i < LENGTH; i++)
                {
                    writers[i] = NULL;
                }
                ToDo(menu2, writers);
            }
            if (Index == 1)
            {
                Console_clear();
                printf("Введіть назву тестового файлу\n");
                char dir[100];
                dir[0] = '\0';
                fgets(dir, 100, stdin);
                dir[strlen(dir) - 1] = '\0';
                char *filePath = dir;
                bool exists = fileExists(filePath);
                if (!exists)
                {
                    printf("Файлу не існує");
                    cleanBuffer();
                    
                }

                else
                {
                    int size = getFileSize(filePath);
                    char gt[size + 1];
                    readFileToBuffer(filePath, gt, size);
                    gt[size] = '\0';
                    int k = 0;

                    struct Writer *writers[LENGTH];
                    for (int i = 0; i < LENGTH; i++)
                    {
                        writers[i] = NULL;
                    }

                    char buffer[300];
                    buffer[0] = '\0';

                    for (int i = 0; i < strlen(gt); i++)
                    {
                        if (gt[i] == '~' || gt[i + 1] == '\0')
                        {
                            if(gt[i+1]=='\n')
                            {
                                i = i + 1;
                            }
                            buffer[k] = '\0';
                            bool fall = true;
                            struct Writer new;
                            bool mark = false;
                            int pointCount = 0;
                            int counter = 0;
                            for (int i = 0; i < strlen(buffer); i++)
                            {
                                if (buffer[i] == ':' && mark == false)
                                {
                                    counter = 1;
                                    mark = true;
                                }
                                else
                                {

                                    if (buffer[i] == '_' && mark == true)
                                    {
                                        counter++;
                                    }
                                    else
                                    {
                                        if (counter > 3)
                                        {
                                            fall = false;
                                            break;
                                        }

                                        if (counter == 0 && isdigit(buffer[i]))
                                        {
                                            fall = false;
                                            break;
                                        }

                                        if (counter == 2 && !isdigit(buffer[i]))
                                        {
                                            fall = false;
                                            break;
                                        }

                                        if (counter == 3 && buffer[i] == '.')
                                        {
                                            pointCount++;
                                            if (pointCount == 2)
                                            {
                                                fall = false;
                                                break;
                                            }
                                        }
                                        else if (counter == 3 && !isdigit(buffer[i]))
                                        {
                                            fall = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (fall == true && counter == 3)
                            {
                                strToWriter(buffer, &new);
                                if (new.data.yearOfBirth > 0 && new.data.yearOfBirth < 2018 && new.data.rating > 0 && new.data.rating < 10)
                                {
                                    struct Writer *one = NULL;
                                    one = newWriter(new.name, new.data.theGreatestPoem, new.data.yearOfBirth, new.data.rating);
                                    addWriter(LENGTH, writers, one);
                                }
                            }
                            k = 0;
                            buffer[0] = '\0';
                        }
                        else
                        {
                            buffer[k] = gt[i];
                            k = k + 1;
                        }
                    }
                    ToDo(menu2, writers);
                }
            }
            break;
        case NO:
        case BACK:
        {
            return EXIT_SUCCESS;
        }
        }
    }
    }
    else{
        if(argc==2){
            char * command = argv[1];
            int commandIndex = getCommandIndex(command);
            if (commandIndex < 0) {
                printf("Неправильний ввід\nНатисніть будь-яку клавішу для продовження\n");
                Console_getChar();
                return EXIT_FAILURE;
            }
            else {if(commandIndex == 0){

                test();
                printf("Всі тести успішно пройдені\nНатисніть будь-яку клавішу для продовження\n");
                Console_getChar();

            }
            else{
                printf("Неправильний ввід\nНатисніть будь-яку клавішу для продовження\n");
                Console_getChar();
               
            }
        }
    }
        else {
        printf("Неправильний ввід\nНатисніть будь-яку клавішу для продовження\n");
        Console_getChar();
}

    }
}

void printMainMenu(char vc[][300], int ex)
{
    for (int i = 0; i < 6; i++)
    {
        if (i == ex)
        {
            Console_setCursorAttribute(FG_INTENSITY_YELLOW);
            puts(vc[i]);
            Console_setCursorAttribute(FG_DEFAULT);
        }
        else
            puts(vc[i]);
    }
}

enum MainCommands getMainCommandsUserInput()
{
    enum MainCommands commands = NO;
    do
    {
        char keyCode = Console_getChar();
        switch (keyCode)
        {
        case 127:
            commands = BACK;
            break;
        case 65:
            commands = UP;
            break;
        case 66:
            commands = DOWN;
            break;
        case 10:
            commands = ENTER;
            break;
        }
    } while (commands == NO);
    return commands;
}
int writerDataToStr(struct WritersData *p, char *buffer, int bufferLength)
{
    return snprintf(buffer, bufferLength, "%s_%i_%.1f",
                    p->theGreatestPoem,
                    p->yearOfBirth,
                    p->rating);
}
int writerToStr(struct Writer *p, char *buffer, int bufferLength)
{
    int nwrite = snprintf(buffer, bufferLength, "%s:", p->name);
    int totalwrite = writerDataToStr(&p->data, buffer + nwrite, bufferLength - nwrite);
    return nwrite + totalwrite;
}

int strToWriterData(const char *str, struct WritersData *p)
{
    int nread = 0;
    sscanf(str, "%99[^_]%*c %i_%f %n",
           p->theGreatestPoem,
           &(p->yearOfBirth),
           &(p->rating),
           &nread);
    return nread;
}

int strToWriter(const char *str, struct Writer *p)
{
    int nread = 0;
    sscanf(str, "%99[^:]%*c%n", p->name, &nread);
    int dataRead = strToWriterData(str + nread, &p->data);
    return dataRead + nread;
}
/*
int strToList(const char * str, struct List * list){
    int nread = 0;
    sscanf(str,"%31[^|]%*c%i%n",list -> nameOfList,&list ->total,&nread);
    int totalRead = nread;
    for( int i = 0; i < list -> total && i < list->WritersArrayCapacity; i++){
        struct Writer * wr = &list->writer[i];
        totalRead += strToWriter(str + totalRead,wr);
        sscanf(str + totalRead,"\n");
    }
    return totalRead;
}*/

struct Writer *newWriter(const char *name, const char *poem, int year, float rating)
{
    struct Writer *newWriter = malloc(sizeof(struct Writer));
    strcpy(newWriter->name, name);
    strcpy(newWriter->data.theGreatestPoem, poem);
    newWriter->data.yearOfBirth = year;
    newWriter->data.rating = rating;
    return newWriter;
}

void freeWriter(struct Writer *writer)
{
    free(writer);
}

void clearArray(int len, struct Writer *array[])
{
    for (int i = 0; i < len; i++)
    {
        array[i] = NULL;
    }
}

int countWriters(int len, struct Writer *array[])
{
    int counter = 0;
    for (int i = 0; i < len; i++)
    {
        if (array[i] == NULL)
            break;
        counter++;
    }
    return counter;
}

bool addWriter(int len, struct Writer *array[], struct Writer *newStudent)
{
    int count = countWriters(len, array);
    if (count == LENGTH)
        return false;
    array[count] = newStudent;
    return true;
}

struct Writer *removeWritersAt(int len, struct Writer *array[], int index)
{
    int count = countWriters(len, array);
    if (index < 0 || index >= count)
        return NULL;
    // @todo remove Student
    struct Writer *WritersToRemove = array[index];
    // free(studentToRemove);
    // array[index] = NULL;
    // @todo shift other students
    for (int i = index + 1; i < count; i++)
    {
        array[i - 1] = array[i];
    }
    array[count - 1] = NULL;
    return WritersToRemove;
}

struct Writer *rewriteWriter(struct Writer **array, int index, char *str)
{
    strToWriter(str, array[index]);
    return EXIT_SUCCESS;
}

struct Writer *rewriteSomething(struct Writer **array, int index, int pos)
{
    Console_clear();
    if (pos == 0)
    {
        char buffer[100];
        buffer[0] = '\0';
        printf("Введіть ім'я та прізвище\n");
        scanf("%s", buffer);
        bool flag = true;
        for (int i = 0; i < strlen(buffer); i++)
        {
            if (!isalpha(buffer[i]))
            {
                flag = false;
                break;
            }
        }
        if (flag)
        {
            array[index]->name[0] = '\0';
            strcpy(array[index]->name, buffer);
        }
        else
        {
            printf("Неправильний ввід");
            cleanBuffer();
           
        }
    }
    if (pos == 1)
    {
        char buffer[100];
        buffer[0] = '\0';
        printf("Введіть назву відомої збірки\n");
        scanf("%s", buffer);
        array[index]->data.theGreatestPoem[0] = '\0';
        strcpy(array[index]->data.theGreatestPoem, buffer);
    }
    if (pos == 2)
    {
        printf("Введіть рік народження\n");
        char str[5];
        fgets(str, 5, stdin);
        str[4]='\0';
        int year;
        bool is = true;
        for (int i = 0; i < strlen(str)-1; i++)
        {
            if (!isdigit(str[i]))
            {
                is = false;
            }
        }
        if (is)
        {
            year = atoi(str);
            if (year >= 0 && year <= 2017)
            {
                array[index]->data.yearOfBirth = year;
            }
            else
            {
                printf("Неправильно введений рік");
                cleanBuffer();
                
            }
        }
        else
        {
            printf("Неправильно введений рік");
            cleanBuffer();
          
        }
    }

    if (pos == 3)
    {
        printf("Введіть оцінку\n");
        char str[5];
        fgets(str, 5, stdin);
        str[strlen(str)-1]='\0';
        bool is = true;
        bool point = true;
        int countP = 0;
        for (int i = 0; i < strlen(str)-1; i++)
        {
            if (isdigit(str[i])){

            }
            else {
                if(str[i]=='.' && point == true){
                point = false;
                countP++;
                }
                else{
                if(str[1]=='\0'){
                    point = true;
                    break;
                }
                else{
                    if(str[i]=='.'){
                        countP++;
                        }
                    else{
                        is = false;
                        break;
                    }
                    
                }
            }
        }
            if((str[1]!='.' && point == false)){
                is = false;
                break;
            }
            else if(countP>1){
                is = false;
                break;
            }
            
            
        }
        if (is)
        {
            float buffer;
            if(point){
                buffer = atoi(str);
            }
            else{
                buffer = atof(str);
            }   
            if (buffer > 0 && buffer < 10)
            {
                array[index]->data.rating = buffer;
            }
            else
            {
                printf("Неправильно введена оцінка");
                cleanBuffer();
               
            }
        }
        else
        {
            printf("Неправильно введена оцінка");
            cleanBuffer();
            
        }
        char c = 0;
        while(c != '\n') {
            c = getchar();
        }
    }
    Console_showCursor();
    return EXIT_SUCCESS;
    
}

void printwriters(int len, struct Writer *arr[])
{
    int count = countWriters(len, arr);
    Console_setCursorPosition(1, 120);
    printf("Count: %i\n", count);
    int n = 3;
    for (int i = 0; i < count; i++)
    {
        Console_setCursorPosition(n, 100);
        printf("(%i) - Name and surname: %s\n", i, arr[i]->name);
        Console_setCursorPosition(n + 1, 100);
        printf("The famoust poem: %s\n", arr[i]->data.theGreatestPoem);
        Console_setCursorPosition(n + 2, 100);
        printf("Year of Birth: %i\n", arr[i]->data.yearOfBirth);
        Console_setCursorPosition(n + 3, 100);
        printf("Rating: %.1f\n", arr[i]->data.rating);
        n = n + 4;
    }
    Console_reset();
}
void printChosedField(int len, struct Writer **p, int index, int ind)
{
    int count = countWriters(len, p);
    Console_setCursorPosition(1, 120);
    printf("Count: %i\n", count);
    int n = 3;
    for (int i = 0; i < count; i++)
    {
        if (i == index)
        {
            if (ind == 0)
            {
                Console_setCursorPosition(n, 100);
                Console_setCursorAttribute(FG_INTENSITY_YELLOW);
                printf("(%i) - Name and surname: %s\n", i, p[i]->name);
                Console_setCursorAttribute(FG_DEFAULT);
                Console_setCursorPosition(n + 1, 100);
                printf("The famoust poem: %s\n", p[i]->data.theGreatestPoem);
                Console_setCursorPosition(n + 2, 100);
                printf("Year of Birth: %i\n", p[i]->data.yearOfBirth);
                Console_setCursorPosition(n + 3, 100);
                printf("Rating: %.1f\n", p[i]->data.rating);
                Console_setCursorAttribute(FG_DEFAULT);
            }
            if (ind == 1)
            {
                Console_setCursorPosition(n, 100);
                printf("(%i) - Name and surname: %s\n", i, p[i]->name);
                Console_setCursorPosition(n + 1, 100);
                Console_setCursorAttribute(FG_INTENSITY_YELLOW);
                printf("The famoust poem: %s\n", p[i]->data.theGreatestPoem);
                Console_setCursorAttribute(FG_DEFAULT);
                Console_setCursorPosition(n + 2, 100);
                printf("Year of Birth: %i\n", p[i]->data.yearOfBirth);
                Console_setCursorPosition(n + 3, 100);
                printf("Rating: %.1f\n", p[i]->data.rating);
            }
            if (ind == 2)
            {
                Console_setCursorPosition(n, 100);
                printf("(%i) - Name and surname: %s\n", i, p[i]->name);
                Console_setCursorPosition(n + 1, 100);
                printf("The famoust poem: %s\n", p[i]->data.theGreatestPoem);
                Console_setCursorPosition(n + 2, 100);
                Console_setCursorAttribute(FG_INTENSITY_YELLOW);
                printf("Year of Birth: %i\n", p[i]->data.yearOfBirth);
                Console_setCursorAttribute(FG_DEFAULT);
                Console_setCursorPosition(n + 3, 100);
                printf("Rating: %.1f\n", p[i]->data.rating);
            }
            if (ind == 3)
            {
                Console_setCursorPosition(n, 100);
                printf("(%i) - Name and surname: %s\n", i, p[i]->name);
                Console_setCursorPosition(n + 1, 100);
                printf("The famoust poem: %s\n", p[i]->data.theGreatestPoem);
                Console_setCursorPosition(n + 2, 100);
                printf("Year of Birth: %i\n", p[i]->data.yearOfBirth);
                Console_setCursorPosition(n + 3, 100);
                Console_setCursorAttribute(FG_INTENSITY_YELLOW);
                printf("Rating: %.1f\n", p[i]->data.rating);
                Console_setCursorAttribute(FG_DEFAULT);
            }
        }
        else
        {
            Console_setCursorPosition(n, 100);
            printf("(%i) - Name and surname: %s\n", i, p[i]->name);
            Console_setCursorPosition(n + 1, 100);
            printf("The famoust poem: %s\n", p[i]->data.theGreatestPoem);
            Console_setCursorPosition(n + 2, 100);
            printf("Year of Birth: %i\n", p[i]->data.yearOfBirth);
            Console_setCursorPosition(n + 3, 100);
            printf("Rating: %.1f\n", p[i]->data.rating);
        }
        n = n + 4;
    }
    Console_reset();
}
void printChosedWriter(int len, struct Writer **p, int index)
{
    int count = countWriters(len, p);
    Console_setCursorPosition(1, 120);
    printf("Count: %i\n", count);
    int n = 3;
    for (int i = 0; i < count; i++)
    {
        if (i == index)
        {
            Console_setCursorAttribute(FG_INTENSITY_YELLOW);
            Console_setCursorPosition(n, 100);
            printf("(%i) - Name and surname: %s\n", i, p[i]->name);
            Console_setCursorPosition(n + 1, 100);
            printf("The famoust poem: %s\n", p[i]->data.theGreatestPoem);
            Console_setCursorPosition(n + 2, 100);
            printf("Year of Birth: %i\n", p[i]->data.yearOfBirth);
            Console_setCursorPosition(n + 3, 100);
            printf("Rating: %.1f\n", p[i]->data.rating);
            Console_setCursorAttribute(FG_DEFAULT);
        }
        else
        {
            Console_setCursorPosition(n, 100);
            printf("(%i) - Name and surname: %s\n", i, p[i]->name);
            Console_setCursorPosition(n + 1, 100);
            printf("The famoust poem: %s\n", p[i]->data.theGreatestPoem);
            Console_setCursorPosition(n + 2, 100);
            printf("Year of Birth: %i\n", p[i]->data.yearOfBirth);
            Console_setCursorPosition(n + 3, 100);
            printf("Rating: %.1f\n", p[i]->data.rating);
        }
        n = n + 4;
    }
    Console_reset();
}

struct Writer *findWriters(int len, struct Writer **array, int year, struct Writer **yearWr)
{
    int count = countWriters(len, array);
    clearArray(count, yearWr);
    int n = 0;
    for (int i = 0; i < count; i++)
    {
        if (array[i]->data.yearOfBirth < year)
        {
            yearWr[n] = NULL;
            addWriter(count, yearWr, array[i]);
            n++;
        }
    }

    return *yearWr;
}

int fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return 0; // false: not exists
    fclose(f);
    return 1; // true: exists
}

long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return -1;         // error opening file
    fseek(f, 0, SEEK_END); // rewind cursor to the end of file
    long fsize = ftell(f); // get file size in bytes
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return 0; // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes; // number of bytes read
}
void ToDo(char menu2[][300], struct Writer **writers)
{
    int Index = 0;
    bool flag = true;
    while (flag)
    {
        Console_clear();
        printMainMenu(menu2, Index);
        printwriters(LENGTH, writers);
        enum MainCommands commands = getMainCommandsUserInput();
        switch (commands)
        {
        case UP:
            Index -= 1;
            if (Index < 0)
            {
                Index = 6 - 1;
            }
            break;
        case DOWN:
            Index = (Index + 1) % 6;
            break;
        case ENTER:
        {

            if (Index == 0)
            {
                int count = countWriters(LENGTH,writers);
                if(count!=10){
                    cleanBuffer();
                    Console_clear();
                    char buffer[300];
                    buffer[0] = 0;
                    struct Writer new;
                    printf("Введіть дані для списку в форматі [Name:FamoustPoem_YearOfBirth_rating]\n");
                    fgets(buffer, 300, stdin);
                    buffer[strlen(buffer) - 1] = '\0';
                    bool fall = true;
                    bool mark = false;
                    int pointCount = 0;
                    int counter = 0;
                    for (int i = 0; i < strlen(buffer); i++)
                    {
                        if (i!=0 && buffer[i] == ':' && mark == false)
                        {
                            counter = 1;
                            mark = true;
                        }
                        else
                        {
    
                            if (buffer[i] == '_' && buffer[i-1]!=':' && mark == true)
                            {
                                counter++;
                            }
                            else
                            {
                                if (counter > 3)
                                {
                                    fall = false;
                                    break;
                                }
    
                                if (counter == 0 && isdigit(buffer[i]))
                                {
                                    fall = false;
                                    break;
                                }
    
                                if (counter == 2 && !isdigit(buffer[i]))
                                {
                                    fall = false;
                                    break;
                                }
    
                                if (counter == 3 && buffer[i] == '.')
                                {
                                    pointCount++;
                                    if (pointCount == 2)
                                    {
                                        fall = false;
                                        break;
                                    }
                                }
                                else if (counter == 3 && !isdigit(buffer[i]))
                                {
                                    fall = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (fall == true && counter == 3)
                    {
                        strToWriter(buffer, &new);
                        if (new.data.yearOfBirth > 0 && new.data.yearOfBirth < 2018 && new.data.rating > 0 && new.data.rating < 10)
                        {
                            struct Writer *one = NULL;
                            one = newWriter(new.name, new.data.theGreatestPoem, new.data.yearOfBirth, new.data.rating);
                            addWriter(LENGTH, writers, one);
                        }
                        else
                        {
                            Console_clear();
                            puts("Неправильний ввід");
                            cleanBuffer();
                            
                        }
                    }
                    else
                    {
                        Console_clear();
                        puts("Неправильний ввід");
                        cleanBuffer();
                        
                    }
                }
                else{
                    printf("Неможливо здійснити дію\nНатисніть будь-яку клавішу для продовження\n");
                    Console_getChar();
                }
                
            }

            if (Index == 1)
            {
                Console_clear();
                int count = countWriters(LENGTH, writers);
                Index = 0;
                if (count != 0)
                {
                    bool flag2 = true;
                    while (flag2)
                    {
                        printMainMenu(menu2, 1);
                        printChosedWriter(LENGTH, writers, Index);
                        commands = getMainCommandsUserInput();
                        switch (commands)
                        {
                        case UP:
                            Index -= 1;

                            if (Index < 0)
                            {
                                Index = count - 1;
                            }
                            break;
                        case DOWN:
                            Index = (Index + 1) % count;
                            break;
                        case ENTER:
                        {

                            removeWritersAt(LENGTH, writers, Index);

                            Index = 1;

                            flag2 = false;

                            break;
                        }
                        break;
                        case NO:
                        case BACK:
                        {
                            flag2 = false;
                            break;
                        }
                        }
                        Console_clear();
                    }
                }
                else{
                    printf("Неможливо здійснити дію\nНатисніть будь-яку клавішу для продовження\n");
                    Console_getChar();
                }
                break;
            }

            if (Index == 2)
            {
                Console_clear();
                int count = countWriters(LENGTH, writers);
                Index = 0;
                if (count != 0)
                {
                    bool flag2 = true;
                    while (flag2)
                    {
                        printMainMenu(menu2, 2);
                        printChosedWriter(LENGTH, writers, Index);
                        commands = getMainCommandsUserInput();
                        switch (commands)
                        {
                        case UP:
                            Index -= 1;

                            if (Index < 0)
                            {
                                Index = count - 1;
                            }
                            break;
                        case DOWN:
                            Index = (Index + 1) % count;
                            break;
                        case ENTER:
                        {
                            cleanBuffer();
                            Console_clear();
                            char buffer[300];
                            buffer[0] = 0;
                            struct Writer new;
                            printf("Введіть дані для списку в форматі [Name:FamoustPoem_YearOfBirth_rating]\n");
                            fgets(buffer, 300, stdin);
                            buffer[strlen(buffer) - 1] = '\0';
                            bool fall = true;
                            bool mark = false;
                            int pointCount = 0;
                            int counter = 0;
                            for (int i = 0; i < strlen(buffer); i++)
                            {
                                if (buffer[i] == ':' && mark == false)
                                {
                                    counter = 1;
                                    mark = true;
                                }
                                else
                                {

                                    if (buffer[i] == '_' && mark == true)
                                    {
                                        counter++;
                                    }
                                    else
                                    {
                                        if (counter > 3)
                                        {
                                            fall = false;
                                            break;
                                        }

                                        if (counter == 0 && isdigit(buffer[i]))
                                        {
                                            fall = false;
                                            break;
                                        }

                                        if (counter == 2 && !isdigit(buffer[i]))
                                        {
                                            fall = false;
                                            break;
                                        }

                                        if (counter == 3 && buffer[i] == '.')
                                        {
                                            pointCount++;
                                            if (pointCount == 2)
                                            {
                                                fall = false;
                                                break;
                                            }
                                        }
                                        else if (counter == 3 && !isdigit(buffer[i]))
                                        {
                                            fall = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (fall == true && counter == 3)
                            {
                                strToWriter(buffer, &new);
                                if (new.data.yearOfBirth > 0 && new.data.yearOfBirth < 2018 && new.data.rating > 0 && new.data.rating < 10)
                                {
                                    strcpy(writers[Index]->name, new.name);
                                    strcpy(writers[Index]->data.theGreatestPoem, new.data.theGreatestPoem);
                                    writers[Index]->data.yearOfBirth = new.data.yearOfBirth;
                                    writers[Index]->data.rating = new.data.rating;
                                }
                                else
                                {
                                    Console_clear();
                                    puts("Неправильний ввід");
                                    cleanBuffer();
                                    
                                }
                            }
                            else
                            {
                                Console_clear();
                                puts("Неправильний ввід");
                                cleanBuffer();
                              
                            }

                            flag2 = false;

                            break;
                        }
                        break;
                        case NO:
                        case BACK:
                        {
                            flag2 = false;
                            break;
                        }
                        }
                        Console_clear();
                    }
                }
                else{
                    printf("Неможливо здійснити дію\nНатисніть будь-яку клавішу для продовження\n");
                    Console_getChar();
                }
            }

            if (Index == 3)
            {
                Console_clear();
                int count = countWriters(LENGTH, writers);
                Index = 0;
                if (count != 0)
                {
                    bool flag2 = true;
                    while (flag2)
                    {
                        printMainMenu(menu2, 3);
                        printChosedWriter(LENGTH, writers, Index);
                        commands = getMainCommandsUserInput();
                        switch (commands)
                        {
                        case UP:
                            Index -= 1;

                            if (Index < 0)
                            {
                                Index = count - 1;
                            }
                            break;
                        case DOWN:
                            Index = (Index + 1) % count;
                            break;
                        case ENTER:
                        {
                            int Ind = 0;
                            bool flag3 = true;
                            while (flag3)
                            {
                                Console_clear();
                                printMainMenu(menu2, 3);
                                printChosedField(LENGTH, writers, Index, Ind);
                                commands = getMainCommandsUserInput();
                                switch (commands)
                                {
                                case UP:
                                    Ind -= 1;

                                    if (Ind < 0)
                                    {
                                        Ind = 4 - 1;
                                    }
                                    break;
                                case DOWN:
                                    Ind = (Ind + 1) % 4;
                                    break;
                                case ENTER:
                                {
                                    rewriteSomething(writers, Index, Ind);
                                    flag3 = false;
                                    break;
                                }
                                break;
                                case NO:
                                case BACK:
                                {
                                    flag3 = false;
                                    break;
                                }
                                }
                            }

                            flag2 = false;

                            break;
                        }
                        case NO:
                        case BACK:
                        {
                            flag2 = false;
                            break;
                        }
                        }
                        Console_clear();
                    }
                }
                else {
                    printf("Неможливо здійснити дію\nНатисніть будь-яку клавішу для продовження\n");
                    Console_getChar();
                }
            }
                if (Index == 4)
                {
                    cleanBuffer();
                    Console_clear();
                    printf("Введіть рік\n");
                    char str[5];
                    str[0]='\0';
                    fgets(str,5,stdin);
                    str[4]='\0';
                    int year;
                    bool is = true;
                    for(int i = 0; i<strlen(str)-1;i++){
                        if (!isdigit(str[i])){
                            is = false;
                        }
                    }
                    if(is){
                        year = atoi(str);
                        int count = countWriters(LENGTH,writers);
                        struct Writer * yearWr[count];
                        for(int i = 0; i < count;i++){
                            yearWr[i] = NULL;
                        }
                        findWriters(LENGTH, writers, year, yearWr);
                        printwriters(count,yearWr);
                        Console_getChar();
                        count = countWriters(count,yearWr);
                    }
                    else{
                        printf("Неправильно введений рік");
                        cleanBuffer();
                        
                    }
                    Console_clear();
                }
                if (Index == 5)
                {
                    
                    int count = countWriters(LENGTH,writers);
                    if(count==0){
                        Console_clear();
                        printf("Неможливо зберегти\nСписок порожній\nВведіть будь-яку клавішу для продовження\n");
                        Console_getChar();
                    }
                    else{
                        cleanBuffer();
                        printf("Введіть назву тестового файлу\n");
                        char dir[100];
                        dir[0] = '\0';
                        fgets(dir, 100, stdin);
                        dir[strlen(dir) - 1] = '\0';
                        int count = countWriters(LENGTH,writers);
                        char text [2000];
                        text[0]='\0';
                        for(int i = 0; i < count;i++){
                            writerToStr(writers[i],&text[0] + strlen(text)+1,300);
                            text[strlen(text)] = '~';
                        }
                        text[strlen(text)] = '\0';
                        saveToFile(text,dir);
                    }
                   
                }
            }
            break;

        case BACK:{
            int cot = countWriters(LENGTH,writers);
            for(int i=0;i<cot;i++){
                freeWriter(writers[i]);
            }
           flag = false;
           Index = 0;
        }
             
            break;
        case NO:
            break;
        
        };
    }
}

void cleanBuffer() {
    char c = 0;
    while(c != '\n') {
        c = getchar();
    }
}

bool isEqualData(struct WritersData a,struct WritersData b){
    if(strcmp(a.theGreatestPoem,b.theGreatestPoem)==0 && a.yearOfBirth==b.yearOfBirth && a.rating == b.rating){
        return true;
    }
    return false;



   
}
bool isEqual(struct Writer a,struct Writer b){
        if(strcmp(a.name,b.name)==0 && isEqualData(a.data,b.data)){
            return true;
        }
        return false;
    
    
    
       
}
bool isEqualWriters(struct Writer first[],struct Writer second[]){
     if(first!=NULL && second!=NULL){
        for(int i = 0;i < 3;i++){
            if(!isEqual(first[i], second[i])){
               return false;
            }
       }
       return true;
    }
    else return false;     
}

void test(){
    struct Writer one = {"",{"",4,2}};
    struct Writer two = {"artem",{"",4,2}};
    struct Writer three = {"artem",{"perfect",2000,9.4}};
    struct Writer four = {"artem",{"perfect",2000,9.4}};
    struct Writer five = {"vova",{"not perfect",1999,9.4}};
    
    assert(isEqual(one,two) == 0);
    assert(isEqual(one,three) == 0);
    assert(isEqual(three,four) == 1);
    assert(isEqual(three,five) == 0);
    assert(isEqual(two,five) == 0);

    struct WritersData ones = {"",4,2};
    struct WritersData twos= {"",4,2};
    struct WritersData threes = {"perfect",2000,9.4};
    struct WritersData fours = {"perfect",2000,9.4};
    struct WritersData fives = {"not perfect",1999,9.4};
    
    assert(isEqualData(ones,twos) == 1);
    assert(isEqualData(ones,threes) == 0);
    assert(isEqualData(threes,fours) == 1);
    assert(isEqualData(threes,fives) == 0);
    assert(isEqualData(twos,fives) == 0);
    struct Writer a[3] = {one,two,three};
    struct Writer * p1 = &a[0];
    struct Writer b[3] = {three,four,five};
    struct Writer * p2 = &b[0];
    struct Writer c[3] = {three,four,five};
    struct Writer * p3 = &c[0];
    struct Writer d[4] = {two,five,four};
    struct Writer * p4 = &d[0];
    struct Writer e[3] = {three,five,four};
    struct Writer * p5 = &e[0];

    assert ( isEqualWriters ( p1 , p2 ) == 0);
    assert ( isEqualWriters ( p2 , p2 ) == 1);
    assert ( isEqualWriters ( p2 , p3 ) == 1);
    assert ( isEqualWriters ( p1 , p3 ) == 0);
    assert ( isEqualWriters ( p1 , p4 ) == 0);
    assert ( isEqualWriters ( p3 , p4 ) == 0);
    assert ( isEqualWriters ( p3 , p5 ) == 0);
    assert ( isEqualWriters ( NULL , p1 ) == 0);
    char buffer[300];
    buffer[0]='\0';



    writerToStr(&three, buffer, 300);

    assert(strcmp(buffer,"artem:perfect_2000_9.4") == 0 );
    assert(strcmp(buffer,"artem:perfect_2000_2.3") != 0 );
    
    struct Writer te;
    strToWriter(buffer,&te);
    
    buffer[0]='\0';

    writerToStr(&five, buffer, 300);

    assert(strcmp(buffer,"vova:not perfect_1999_9.4") == 0 );
    assert(strcmp(buffer,"artem:perfect_2000_2.3") != 0 );

    struct Writer pe;
    strToWriter(buffer,&pe);

    assert(isEqual(pe,five)==1);
    assert(isEqual(pe,four)==0);
    assert(isEqual(te,five)==0);
    assert(isEqual(te,four)==1);
    assert(isEqual(pe,te)==0);

    

}