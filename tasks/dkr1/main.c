#include <stdlib.h>
#include <stdio.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <time.h>
#include <progbase.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

int indexofSecSentence(char*str); 
void copyOfFirstSentense(char*str,char*buffer, int len);
int fileExists(const char *fileName);
long getFileSize(const char *fileName);
int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);

struct Sentense{
    int index;
    char Sentense[500];
};
struct Sentense texts[100];
struct SentenseArray{
    struct Sentense * text;
    int lenght;
};


bool isEqual(struct Sentense first,struct Sentense second);
bool isEqualArrays(struct SentenseArray*first,struct SentenseArray*second);

struct Sentense firstSentense(char*str); 

struct SentenseArray * getAllSentencesArrayNew(const char * str);

void freeArray(struct SentenseArray * array);

#define __LEN(A) sizeof(A) / sizeof(A[0])

char*const ALLOWED_COMMANDS [] = {
    "task1",
    "task2",
    "task3",
    "task4"
};

int getCommandIndex(const char * command){
    for ( int i = 0; i < __LEN(ALLOWED_COMMANDS);i++){
        char * allowedCommand = ALLOWED_COMMANDS[i];
        if ( strcmp( command, allowedCommand) == 0){
            return i;
        }
    }
    return -1;
}

void task1();
void task2();
void task3();

int main(int argc, char*argv[]){

    if (argc == 1) return EXIT_FAILURE;
    else{
        char * command = argv[1];
        int commandIndex = getCommandIndex(command);
        if (commandIndex < 0) return EXIT_FAILURE;
        else {
            if (commandIndex < 3){
                if(argc!=2) return EXIT_FAILURE;
                else {
                    if (commandIndex == 0) task1();
                    if (commandIndex == 1) task2();
                    if (commandIndex == 2) task3();
                }
            } 
            else if(commandIndex == 3){
                if (argc != 3) return EXIT_FAILURE;
                char * filePath = argv[2];
                bool exists = fileExists(filePath);
                if (!exists) return EXIT_FAILURE;
               
                else{
                    int size = getFileSize(filePath);
                    char gt[size + 1];
                    readFileToBuffer(filePath, gt, size);
                    gt[size]='\0';
                    int k = 0;
                    struct SentenseArray * aww = getAllSentencesArrayNew(gt);
                    puts("--------------------------------------------------------------------------------------------------------------");
                    puts("Шкала розфарбовування речень: менше 10 символів - червоний, від 10 до 25 - жовтий, більше 25 - зелений");
                    puts("--------------------------------------------------------------------------------------------------------------");
                    for(int i = 0; i < aww->lenght;i++){
                        
                        k = strlen(aww->text[i].Sentense);
        
                        if(k < 10){
                            Console_setCursorAttribute(FG_RED);
                        }
                        else{
                            if (k<25){
                                Console_setCursorAttribute(FG_YELLOW);
                            }
                            else{
                                Console_setCursorAttribute(FG_GREEN);
                            }
                        } 
                        puts(aww->text[i].Sentense);
                        Console_setCursorAttribute(FG_DEFAULT);
                    }
                    char letters[]={"aeiyuoAEIYUO"};
                    puts("--------------------------------------------------------------------------------------------------------------");
                    puts("Окремо вивести список тих речень, у яких більше 10 символів, а перше слово починається на приголосну літеру");
                    puts("--------------------------------------------------------------------------------------------------------------");
                    for(int i = 0; i < aww->lenght;i++){

                        k = strlen(aww->text[i].Sentense);

                        if (k > 10){

                            for(int j = 0; j < k; j++){

                                if(isalpha(aww -> text[i].Sentense[j]))
                                {
                                    if (!strchr(letters,aww -> text[i].Sentense[j])){
                                    puts(aww->text[i].Sentense);
                                    }
                                    j = k;
                                }
                            }
                        }
                    }    


                    freeArray(aww);
                    
                }
            }
        }
    }
    
    
    /*
    // TESTS FOR TASK 1;
    task1();

    // TESTS FOR TASK 2; 
    task2();

    // TESTS FOR TASK 3;
    task3();
    */
    
  return EXIT_SUCCESS;
}

int indexofSecSentence(char*str){
    char cha[]={".!?"};
    if(str!=NULL){
    int m = strlen(str);
    for(int i = 0; i < m; i++){
        if (strchr(cha,str[i])){
            while(strchr(cha,str[i+1])&&i+1<m){
                i++;
            }
            if (strchr(cha,str[i+1])==0){
             return i+1;
            }
            else {
            return -1;
            }
        }
    }
}
    return -1;
}


void copyOfFirstSentense(char*str,char*buffer, int len){
    char cha[]={".!?"};
    int n = 0;
    if(str!=NULL){
        int m = strlen(str);
        for(int i = 0; i < m; i++){
            if (strchr(cha,str[i])==0&&n<len){
                buffer[n] = str[i];
                n++;
            }
            else{
            break;
            }
        }
    }
    buffer[n] = '\0';
}

bool isEqual(struct Sentense first,struct Sentense second){
    if(first.index == second.index && strcmp(first.Sentense,second.Sentense)==0){
        return true;
    }
    return false;
}

bool isEqualArrays(struct SentenseArray*first,struct SentenseArray*second){
    if(first!=NULL && second!=NULL){
    if(first->lenght == second->lenght){
        for(int i = 0;i<(first->lenght);i++){
            if(!isEqual(first->text[i], second->text[i])){
               return false;
            }
       }
       return true;
    }     
}
    return false;
}

struct Sentense firstSentense(char*str){
    struct Sentense first;
    first.index = 0;
    copyOfFirstSentense(str,first.Sentense,500);
    return first; 
}



struct SentenseArray * getAllSentencesArrayNew(const char * str){
   
    if(str !=NULL){
    char b[strlen(str)];
    strcpy(b,str);
   
    char * pstr = b;
    char*p2str = pstr;
    int n = 0;
   int lenght = 0;
   while(n != -1){
        lenght = lenght + 1;
        n = indexofSecSentence(p2str);
        if ( n != -1){
        p2str =  p2str + n;
        }
    }
    struct SentenseArray*heap = malloc(sizeof(struct SentenseArray));
    heap->text = malloc(lenght * sizeof(struct Sentense));
    heap->lenght = lenght;
    int index = 0;
    heap->text[0].index = index;
    for (int i = 0; i<heap->lenght;i++){
        copyOfFirstSentense(pstr,heap->text[i].Sentense,500);
       // puts(heap->text[i].Sentense);
        heap->text[i].index = index;
        index = index + n;
        n = indexofSecSentence(pstr);       
        pstr = pstr + n;
        
    }
    return heap;
}
else return NULL;
}

void freeArray(struct SentenseArray * array) {
    free(array->text);  // 
    free(array);  // 
}

void task1(){
    int len = 400;
    char buffer[len];
    buffer[0] = '\0';

        assert(indexofSecSentence("aasfwrh, gsfgsgd s hdhf.h") == 24);
        assert(indexofSecSentence("a.asfwrh, gsfgsgd s hdhfh") == 2);
        assert(indexofSecSentence(".aasfwrh, gsfgsgd s hdhfh") == 1);
        assert(indexofSecSentence("!.?aasfwrh, gsfgsgd s hdhfh") == 3);
        assert(indexofSecSentence("aasfwrh, gsfgsgd s hdhfh") == -1);
        assert(indexofSecSentence(NULL) == -1);
        
        
        copyOfFirstSentense("aasfwrh, gsfgsgd s hdhf.h", buffer, 50);
        assert(strcmp(buffer,"aasfwrh, gsfgsgd s hdhf")==0);
        buffer[0] = '\0';
    
        copyOfFirstSentense("aasfwrh, gsfgsgd s hdh.fh",buffer, 50);
        assert(strcmp(buffer,"aasfwrh, gsfgsgd s hdh")==0);
        buffer[0] = '\0';
    
        copyOfFirstSentense(".aasfwrh, gsfgsgd s hdhfh",buffer, 50);
        assert(strcmp(buffer,"")==0);
        buffer[0] = '\0';
    
        copyOfFirstSentense(NULL,buffer, len);
        assert(strcmp(buffer,"")==0);
        buffer[0] = '\0';
    
        copyOfFirstSentense("aasfwrh, gsfgsgd s hdhfh",buffer, 5);
        assert(strcmp(buffer,"aasfw")==0);
        buffer[0] = '\0';
}

void task2(){
                                       // Comparing Sentenses

        struct Sentense one = {0,"you are here"};
        struct Sentense two = {0,"you are here"};
        struct Sentense three = {1,"you are here"};
        struct Sentense four = {0,"you are here "};
        assert(isEqual(one,one)==1);
        assert(isEqual(one,two)==1);
        assert(isEqual(one,three)==0);
        assert(isEqual(one,four)==0);
        assert(isEqual(three,four)==0);
                                                 
                                          //Comparing SentensesArrays 
                                
        struct Sentense a[3] = {one,two,three};
        struct Sentense b[3] = {one,two,three};
        struct Sentense c[3] = {one,two,four};
        struct Sentense d[4] = {one,two,three,four};
        struct Sentense e[3] = {two,one,three};

        struct SentenseArray a1 = { &a[0], 3};
        struct SentenseArray a2 = { &b[0], 3};
        struct SentenseArray a3 = { &c[0], 4};
        struct SentenseArray a4 = { &d[0], 4};
        struct SentenseArray a5 = { &e[0], 3};

        assert ( isEqualArrays ( &a1 , &a2 ) == 1);
        assert ( isEqualArrays ( &a1 , &a1 ) == 1);
        assert ( isEqualArrays ( &a2 , &a2 ) == 1);
        assert ( isEqualArrays ( &a1 , &a3 ) == 0);
        assert ( isEqualArrays ( &a1 , &a4 ) == 0);
        assert ( isEqualArrays ( &a3 , &a4 ) == 0);
        assert ( isEqualArrays ( &a1 , &a5 ) == 1);
        assert ( isEqualArrays ( NULL , &a1 ) == 0);

                                            //testing function firstSentense                                    
        char text[]={"At 00:00, Cinderella ran away from her prince, leaving a glass slipper on the marbled steps of the grand staircase. At 00:00, I ran away from him. But unlike Cinderella, I didn't leave a single thing behind."};
        struct Sentense test1 = {0,"At 00:00, Cinderella ran away from her prince, leaving a glass slipper on the marbled steps of the grand staircase"};
        struct Sentense test2 = {1,"At 00:00, Cinderella ran away from her prince, leaving a glass slipper on the marbled steps of the grand staircase"};
        struct Sentense test3 = {0,"At 00:00, Cinderella ran away from her prince, leaving a glass slipper on the marbled steps of the grand staircase."}; 
        assert (isEqual(firstSentense( text ), test1 ) == 1);
        assert (isEqual(firstSentense( text ), test2 ) == 0);
        assert (isEqual(firstSentense( text ), test3 ) == 0);
        assert (isEqual(firstSentense( text ), test3 ) == 0);
        assert (isEqual(firstSentense( NULL ), test3 ) == 0);
    
}

void task3(){
    const char tex[]={"At 00:00, Cinderella ran away from her prince, leaving a glass slipper on the marbled steps of the grand staircase. At 00:00, I ran away from him. But unlike Cinderella, I didn't leave a single thing behind."};
    const char tex2[]={"At 00:00, I ran away from him. But unlike Cinderella, I didn't leave a single thing behind."};
    
    struct SentenseArray*p = getAllSentencesArrayNew(tex);
    struct SentenseArray*z = getAllSentencesArrayNew(tex);
    struct SentenseArray*m = NULL;
    struct SentenseArray*i = getAllSentencesArrayNew(tex2);
    assert( isEqualArrays ( p , p ) == 1);
    assert( isEqualArrays ( p , z ) == 1);
    assert( isEqualArrays ( p , m ) == 0);
    assert( isEqualArrays ( p , i ) == 0);
   
   freeArray(z);
   freeArray(p);
  // freeArray(m);
   freeArray(i);
}


int fileExists(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    fclose(f);
    return 1;  // true: exists
}

long getFileSize(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  // number of bytes read
}