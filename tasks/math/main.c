#include <stdio.h>
#include <math.h>
#include <progbase.h>
int main(void)
{
    double x;
    double F1 = 0;
    double F2 = 0;

    for (x = -10; x <= 10; x = x + 0.5)
    {
        printf("x = %lf\n", x);
        if (sin(x - 5) == 0)
        {
            puts("F1(x) Can't be computed");
            if (x + 5 < 0)
            {
                puts("F2(x) Can't be computed");
            }
            else
            {
                F2 = sqrt(x + 5) - 7;
                printf("F2(x)=%lf\n", F2);
            }
        }
        else
        {
            F1 = 0.5 * cos(x - 5) / sin(x - 5);
            printf("F1(x)=%lf\n", F1);

            if (x + 5 < 0)
            {
                puts("F2(x) Can't be computed");
            }
            else
            {
                F2 = sqrt(x + 5) - 7;
                printf("F2(x)=%lf\n", F2);
                printf("F1(x)+F2(x)=%lf\n", F2 + F1);
                printf("F1(x)*F2(x)=%lf\n", F2 * F1);
                if (F2 != 0)
                {
                    printf("F1(x)/F2(x)=%lf\n", F1 / F2);
                }
                else
                {
                    puts("F1(x) / F2(x) Division by zero");
                }
                if (F1 != 0)
                {
                    printf("F2(x)/F1(x)=%lf\n", F2 / F1);
                }
                else
                {
                    puts("F2(x) / F1(x) Division by zero");
                }
            }
        }

        puts("-------------------------");
    }
    return 0;
}