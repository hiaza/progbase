#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>

int main(void)
{
    /* colors encoding table */
    const unsigned long MILLIS = 200;
    const char colorsTable[16][2] = {
        {'0', BG_BLACK},
        {'1', BG_INTENSITY_BLACK},
        {'2', BG_GREEN},
        {'3', BG_INTENSITY_RED},
        {'4', BG_CYAN},
        {'5', BG_MAGENTA},
        {'6', BG_YELLOW},
        {'7', BG_WHITE},
        {'8', BG_RED},    
        {'9', BG_BLUE},        
        {'A', BG_INTENSITY_GREEN},
        {'B', BG_INTENSITY_YELLOW},
        {'C', BG_INTENSITY_BLUE},
        {'D', BG_INTENSITY_MAGENTA},
        {'E', BG_INTENSITY_CYAN},
        /* @todo add other colors pairs here */
        {'F', BG_INTENSITY_WHITE}};
    int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    char colorsPalette[] = "0123456789ABCDEF";
    int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
    int i = 0;
    int colorPairIndex = 0;
    Console_clear();
    for (i = 0; i < colorsPaletteLength; i++)
    {
        char colorCode = '\0';
        char color = '\0';
        /* get current color code from colorsPalette */
        colorCode = colorsPalette[i];
        /* find corresponding color in table */
        for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
        {
            char colorPairCode = colorsTable[colorPairIndex][0];
            char colorPairColor = colorsTable[colorPairIndex][1];
            if (colorCode == colorPairCode)
            {
                color = colorPairColor;
                break; /* we have found our color, break the loop */
            }
        }
        /* print space with founded color background */
        Console_setCursorAttribute(color);
        putchar(' ');
    }

    puts("");

    char image[28][28] = {
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { 'F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','F','F','0','0','0','0','0','0','0','0','0','0','0','0','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','0','0','9','9','9','9','9','9','9','9','9','9','9','9','0','0','F','F','F','F','F' },
        { 'F','F','F','F','F','0','0','F','F','F','F','F','F','F','F','F','F','9','9','9','9','9','9','0','0','F','F','F' },
        { 'F','F','F','0','0','0','0','F','F','F','F','F','F','0','0','F','F','F','F','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','0','0','F','F','0','0','F','F','0','0','F','F','0','0','F','F','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','0','0','0','0','0','0','F','F','F','F','0','0','0','0','F','F','9','9','9','9','9','9','0','0','F' },
        { 'F','0','0','6','6','6','6','6','6','6','6','F','F','F','F','F','F','F','F','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','0','0','F','F','F','F','F','F','F','F','F','F','F','F','F','F','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','0','0','F','F','F','F','F','F','F','F','F','F','0','0','0','0','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','0','0','F','F','F','F','F','F','F','F','0','0','9','9','9','9','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','0','0','F','F','F','F','F','F','0','0','9','9','9','9','9','9','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','0','0','F','F','F','F','F','F','F','F','0','0','0','0','0','0','9','9','9','9','9','9','0','0','F' },
        { 'F','F','F','F','F','0','0','F','F','F','F','F','F','F','F','F','F','F','F','0','0','9','9','0','0','F','F','F' },
        { 'F','F','F','F','F','F','F','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F','F' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' }
    };
    for (int i = 0;i<28;i++){
        for  (int j = 0; j<28; j++){
            for (int e = 0; e < 16; e++ ){
                if (colorsTable[e][0]==image[i][j]){
                    Console_setCursorAttribute(colorsTable[e][1]);
                putchar(' ');
                
                }
            }
            

        }
        puts("");
    }
    
    Console_setCursorAttribute(BG_DEFAULT);
    int size = 28;
    int  j;
    int x, y;
    int count = 0;
    int idir = 0;
    int jdir = 0;
    int lim;

    /* initial values */
    i = 14;
    j = 14;
    idir = 0;
    jdir = -1;
    lim = 13;
    for (count = 0; count < size * size; count++) {
        /* output */
        x = 1 + j;
        y = 31 + i;
        Console_setCursorPosition(y, x);
        for (int e = 0; e < 16; e++ ){
            if (colorsTable[e][0]==image[i][j]){
                Console_setCursorAttribute(colorsTable[e][1]);
            putchar(' ');
            fflush(stdout);  /* force console output */
            sleepMillis(20);
            }
           
        }
        /* end output */

        /* move next */
        i += idir;
        j += jdir;

        /* limits */
        if (j == -1 + lim) {
            j++;
            i--;
            jdir = 0;
            idir = -1;
        }
        if (i == -1 + lim) {
            lim--;
            i++;
            j++;
            idir = 0;
            jdir = 1;
        }
        if (j == size - lim) {
            j--;
            i++;
            idir = 1;
            jdir = 0;
        }
        if (i == size-lim && j == size - lim -1) {
            j--;
            i--;
            idir = 0;
            jdir = -1;
        }
    
    
    }
    Console_setCursorPosition( 1 + (2*size + 1), 1);
    Console_setCursorAttribute(BG_DEFAULT);
int tmp;
    for (i = 0; i < size; i++) {
        for (j = 0; j < size / 2; j++) {
            tmp = image[i][j];
            image[i][j] = image[i][size - 1 - j];
            image[i][size - 1 - j] = tmp;
        }
    }
    for (int i = 0;i<28;i++){
        for  (int j = 0; j<28; j++){
            for (int e = 0; e < 16; e++ ){
                if (colorsTable[e][0]==image[i][j]){
                    Console_setCursorAttribute(colorsTable[e][1]);
                putchar(' ');
                }
            }
            

        }
        puts("");
    }
    Console_setCursorAttribute(BG_DEFAULT);
    Console_reset();
    return 0;
}