#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

char *ns_convert(
    char *buffer,
    const char *number,
    unsigned int sourceBase,
    unsigned int destBase);
int main()
{
    char buf[1000] = "";
    char * x = NULL;
    x = ns_convert(buf, "1A", 16, 10); /* x = "26" */
    puts(x);
    x = ns_convert(buf, "1A", 10, 2);
    puts(x); 
    return 0;
}

char *ns_convert(
    char *buffer,
    const char *number,
    unsigned int sourceBase,
    unsigned int destBase)
{

    int s = 1000;
    char b[s];
    b[0] = 0;
    double sum = 0;
    int m = strlen(number);
    bool flag = true;
    int n = 0;
    while (flag)
    {
        if (number[n] == '.' || n == m)
        {
            flag = false;
        }
        else
            n++;
    }
    int d = m - n;
    flag = true;
   
    if (d>12){
        m = n + 13;
    }
   
    if (m > 64 || sourceBase > 36 || sourceBase < 2 || destBase > 36 || destBase < 2)
    {
        buffer[0] = '\0';
    }
    else
    {
        for (int i = 0; i < m; i++)
        {

            if (number[i] == '.')
            {
                flag = false;
            }
            if (flag)
            {
                int p;
                if (number[i] >= 'A')
                {
                    p = number[i] - 'A' + 10;
                }
                else
                {
                    p = number[i] - '0';
                }
                if (p >= sourceBase)
                {
                    buffer[0] = '\0';
                    return buffer;
                }
                n = n - 1;
                sum = sum + p * pow(sourceBase, n);
            }
            flag = true;
        }

        int i = 0;
        double r = sum - (int)sum;
        int k = sum - r;

        while (k != 0)
        {
            int reminder = k % destBase;
            k = k / destBase;
            if (reminder >= 10)
                b[i] = reminder + 'A' - 10;
            else
                b[i] = reminder + '0';
            i++;
        }
        b[i] = '\0';
        int L = strlen(b) - 1;
        for (i = 0; L >= 0; L--, i++)
        {
            buffer[i] = b[L];
        }
        if (r == 0)
        {
            buffer[i] = '\0';
        }
        else
        {
            buffer[i] = '.';
            double drib = r;
            L = 0;
            while (L < 6)
            {
                double p = drib * destBase;
                double ost = p - (int)p;
                int rez = p - ost;
                drib = p - (int)p;
                if (rez >= 10)
                    buffer[i + 1] = rez + 'A' - 10;
                else
                    buffer[i + 1] = rez + '0';
                i = i + 1;
                L = L + 1;
            }
            buffer[i + 1] = '\0';
        }
    }

    return buffer;
}