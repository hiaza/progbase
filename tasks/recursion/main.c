#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

int isEqual(const int Counter1, const int Counter2);
int counter(const char *const str,const char sumbol);

int main(void)
{

    assert ( counter ( "Shall we begin?" , '{' ) == 0);
    assert ( counter ( "{Shall we begin?}" , '{' ) == 1);
    assert ( counter ( "{Shall we begin?}" , '}' ) == 1);
    assert ( counter ( "{{{{Shall we begin?" , '{' ) == 4);
    assert ( counter ( "" , '{' ) == 0);

    assert ( isEqual(counter ( "" , '{' ),counter ( "Shall we begin?" , '{' )) == 1);
    assert ( isEqual(counter ( "{Shall we begin?}" , '{' ),counter ( "{Shall we begin?}" , '}' )) == 1);
    assert ( isEqual(counter ( "{Shall we begin?}" , '{' ),counter ( "{{{{Shall we begin?}" , '{' )) == 0);
    
    int counter1 = 0;
    int counter2 = 0;
    char str[100];
    printf("Please, enter a string: ");
    fgets(str, 100, stdin);
    str[strlen(str) - 1] = '\0';
    printf("You've entered: %s\n", str);

    counter1 = counter(str,'{');
    counter2 = counter(str,'}');
    
    printf("\nTask: За допомогою рекурсивних функцій (без використання циклів):обчислити і вивести 1 якщо\nкількість відкриваючих фігурних дужок у строці рівна кількості закриваючих круглих дужок,\nінакше 0 у консоль.\n \nThe result is : %i\n\n", isEqual(counter1,counter2));
    return EXIT_SUCCESS;
}
int counter(const char *const str,const char sumbol){
    const char firstChar = str[0];
	if (firstChar == '\0') {
		return 0;
	} else if (firstChar == sumbol) {
		return 1 + counter(str + 1,sumbol);
	} else {
		return counter(str+1,sumbol);
	}
}

int isEqual(const int Counter1, const int Counter2)
{
    if (Counter1 == Counter2 ){
        return 1;
    }
    else return 0;
}
