#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

int main(void)
{

    Console_clear();
    puts("=========================================================================================================================================================================================");
    Console_setCursorAttribute(BG_INTENSITY_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    puts("Завдання №1 (Вивести текст повністю у консолі та загальну кількість символів у тексті)");
    Console_setCursorAttribute(BG_DEFAULT);
    Console_setCursorAttribute(FG_DEFAULT);
    char text[] = "Life at Castle Black followed certain patterns the mornings were for swordplay, the afternoons for work. The black brothers set new recruits to many different tasks, to learn where their skills  lay. Jon cherished the rare afternoons when he was sent out with Ghost ranging at his side to bring back game for the Lord Commander's table, but for every day spent hunting, he gave a dozen to Donal Noye in the armory, spinning the whetstone while the one-armed smith sharpened axes grown dull from use, or pumping the bellows as Noye hammered out a new sword. Other times he ran messages, stood at guard, mucked out stables, fletched arrows, assisted Maester Aemon with his birds or Bowen Marsh with his counts and inventories.";
    int m = strlen(text);
    int glength = 1000;
    char g[glength];
    puts(text);
    printf("Кількість символів = %i\n", m);
    puts("=========================================================================================================================================================================================");
    Console_setCursorAttribute(BG_INTENSITY_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    puts("Завдання №2 (Вивести текст без голосних літер та кількість виведених символів)");
    Console_setCursorAttribute(BG_DEFAULT);
    Console_setCursorAttribute(FG_DEFAULT);
    int indG = 0;
    char d[]={"aeyuioAEIOUY"};
    for (int i = 0; i < m; i++)
    {
      if (strchr(d,text[i])==0){        
            g[indG] = text[i];
            indG++;
        }
    }
    puts(g);
    printf("Кількість символів = %i\n", indG);
    puts("=========================================================================================================================================================================================");
    Console_setCursorAttribute(BG_INTENSITY_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    puts("Завдання №3 (Вивести всі речення тексту, кожне речення із нового рядка та із відміткою про кількість символів у виведеному реченні. Речення розділяти спеціальними рядками (наприклад: ------))");
    Console_setCursorAttribute(BG_DEFAULT);
    Console_setCursorAttribute(FG_DEFAULT);
    indG = 0;
    for (int i = 0; i < m; i++)
    {
        if (text[i] != '.')
        {
            g[indG] = text[i];
            indG++;
        }
        else
        {
            g[indG + 1] = '.';
            g[indG + 2] = '\0';
            puts(g);
            printf("Кількість символів = %i\n", indG);
            puts("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            indG = 0;
        }
    }
    puts("=========================================================================================================================================================================================");
    Console_setCursorAttribute(BG_INTENSITY_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    puts("Завдання №4 (Вивести загальну кількість слів у тексті (всі слова - неперервні послідовності літер))");
    Console_setCursorAttribute(BG_DEFAULT);
    Console_setCursorAttribute(FG_DEFAULT);
    int counter = 0;
    int Num = 0;
    for (int i = 0; i < m; i++)
    {
        if (isalpha(text[i]))
        {
            counter++;
        }
        else
        {
            if (counter > 0)
            {
                Num++;
                counter = 0;
            }
        }
    }
    printf("Кількість слів = %i\n", Num);
    puts("=========================================================================================================================================================================================");
    Console_setCursorAttribute(BG_INTENSITY_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    puts("Завдання №5 (У одному рядку, через кому і один пробіл, вивести всі слова, що мають довжину > 5 символів. Також вивести загальну кількість таких слів)");
    Console_setCursorAttribute(BG_DEFAULT);
    Console_setCursorAttribute(FG_DEFAULT);
    counter = 0;
    Num = 0;
    int n = 500;
    char words[n];
    words[0] = 0;
    for (int i = 0; i < m; i++)
    {
        if (isalpha(text[i]))
        {
            g[counter] = text[i];
            counter++;
        }
        else
        {
            if (counter > 5)
            {
                g[counter] = ',';
                g[counter + 1] = ' ';
                g[counter + 2] = '\0';
                Num++;
                counter = 0;
                strcat(words, g);
            }
            else
            {
                counter = 0;
            }
        }
    }
    puts(words);
    printf("Кількість слів = %i\n", Num);
    puts("=========================================================================================================================================================================================");
    Console_setCursorAttribute(BG_INTENSITY_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    puts("Завдання №6 (У одному рядку, через кому і один пробіл, вивести всі слова, у яких починаються та закінчуються на голосну літеру. Також вивести загальну кількість таких слів)");
    Console_setCursorAttribute(BG_DEFAULT);
    Console_setCursorAttribute(FG_DEFAULT);
    int length = 500;
    counter = 0;
    Num = 0;
    char words2[length];
    words2[0] = 0;
    
    for (int i = 0; i < m; i++)
    {
        if (isalpha(text[i]))
        {
            g[counter] = text[i];
            counter++;
        }
        else
        {
            if (strchr(d,g[0])!=0 && strchr(d,g[counter - 1]) != 0 && isalpha(g[0]) && isalpha(g[counter - 1]))
            {
                g[counter] = ',';
                g[counter + 1] = ' ';
                g[counter + 2] = '\0';
                Num++;
                counter = 0;
                strcat(words2, g);
            }
            else
            {
                counter = 0;
            }
        }
    }
    puts(words2);
    printf("Кількість слів = %i\n", Num);
    return 0;
}