#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <math.h>

int fileExists(const char *fileName);
long getFileSize(const char *fileName);
int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);
void decode(char *message, char *key);
void encode(char *message, char *key);
void saveToFile(char*text, const char*fileName){
    FILE*file = fopen(fileName,"w");
    fputs(text,file);
    fclose(file);
}
#define __LEN(A) sizeof(A) / sizeof(A[0])

char *const ALLOWED_COMMANDS[] = {
    "-k",
    "-d",
    "-f",
    "-o"};
int getCommandIndex(const char *command)
{
    for (int i = 0; i < __LEN(ALLOWED_COMMANDS); i++)
    {
        char *allowedCommand = ALLOWED_COMMANDS[i];
        if (strcmp(command, allowedCommand) == 0)
        {
            return i;
        }
    }
    return -1;
}
int main(int argc, char *argv[])
{
bool flagK = true;
bool flagD = true;
bool flagF1 = true;
bool flagF2 = true;
bool flagO = true;
char fmessage[10000];
char*message = NULL;
char*key = NULL;
char*Name = NULL;
    for(int i = 1; i<argc; i++){
        char *command = argv[i];
        int commandIndex = getCommandIndex(command);
        if (commandIndex == 0){
            if(flagK){
                flagK = false;
            }
            else{
                return EXIT_FAILURE;
            }
            if (i+1<argc){
                key = argv[i+1];
                if (key[0]==*"-"){
                    return EXIT_FAILURE;
                }
                i = i + 1;
            }
            else return EXIT_FAILURE;
        }
        else{
            if(commandIndex == 1){
                if(flagD){
                    flagD = false;
                }
                else{
                    return EXIT_FAILURE;
                }
            }
               
            
            else{
                if(commandIndex == 2){
                    if(flagF1 && flagF2){
                        flagF1 = false;
                    }
                    else{
                        return EXIT_FAILURE;
                    }
                    if (i+1<argc){
                        char *filePath = argv[i+1];
                        bool exists = fileExists(filePath);
                        if (!exists)
                            return EXIT_FAILURE;
        
                        else
                        {
                            int size = getFileSize(filePath);
                            char words[size + 1];
                            readFileToBuffer(filePath, words, size);
                            words[size] = '\0';
                            strcpy(fmessage,words);
                        }
                        i = i + 1;
                    }
                    else return EXIT_FAILURE;

                }
                else {
                    if(commandIndex == 3){
                        if(flagO){
                            flagO = false;
                        }
                        else{
                            return EXIT_FAILURE;
                        }
                        if (i+1<argc){
                            Name = argv[i+1];
                            i = i+1;
                        }
                        else return EXIT_FAILURE;
                    }
                    else{
                        if (flagF2 && flagF1){
                            message = argv[i];
                            flagF2 = false;
                        }
                        else return EXIT_FAILURE;
                    }
                }
            }
        } 
    }
    if (flagK==false && flagF2==false && flagD==false && flagO == false){
        encode(message, key);
        saveToFile(message,Name);
        return EXIT_SUCCESS;
    }
    else{
        if(flagK==false && flagF2==false && flagD==false && flagO){
            encode(message, key);
            puts(key);
            puts(message);
            return EXIT_SUCCESS;
        }
        else {
            if(flagK==false && flagF2==false && flagD && flagO==false){
                decode(message, key);
                saveToFile(message,Name);
                return EXIT_SUCCESS;
            }
            else{
                if(flagK==false && flagF2==false && flagD && flagO){
                    decode(message, key);
                    puts(key);
                    puts(message);
                    return EXIT_SUCCESS;
                }
                else {
                    if (flagK==false && flagF1==false && flagD==false && flagO == false){
                        encode(fmessage, key);
                        saveToFile(fmessage,Name);
                        return EXIT_SUCCESS;
                    }
                    else{
                        if(flagK==false && flagF1==false && flagD==false && flagO){
                            encode(fmessage, key);
                            puts(key);
                            puts(fmessage);
                            return EXIT_SUCCESS;
                        }
                        else {
                            if(flagK==false && flagF1==false && flagD && flagO==false){
                                decode(fmessage, key);
                                saveToFile(fmessage,Name);
                                return EXIT_SUCCESS;
                            }
                            else{
                                if(flagK==false && flagF1==false && flagD && flagO){
                                    decode(fmessage, key);
                                    puts(key);
                                    puts(fmessage);
                                    return EXIT_SUCCESS;
                                }
                                else return EXIT_FAILURE;
                            }
                        }
                    }
                }
            }
        }
    } 
}

    int fileExists(const char *fileName)
    {
        FILE *f = fopen(fileName, "rb");
        if (!f)
            return 0; // false: not exists
        fclose(f);
        return 1; // true: exists
    }

    long getFileSize(const char *fileName)
    {
        FILE *f = fopen(fileName, "rb");
        if (!f)
            return -1;         // error opening file
        fseek(f, 0, SEEK_END); // rewind cursor to the end of file
        long fsize = ftell(f); // get file size in bytes
        fclose(f);
        return fsize;
    }

    int readFileToBuffer(const char *fileName, char *buffer, int bufferLength)
    {
        FILE *f = fopen(fileName, "rb");
        if (!f)
            return 0; // read 0 bytes from file
        long readBytes = fread(buffer, 1, bufferLength, f);
        fclose(f);
        return readBytes; // number of bytes read
    }

    void decode(char *message, char *key)
    {
        
        int j = 0;
        for(int i = 0; i < strlen(key);i++){
            key[i] = toupper(key[i]);
        }
        for (int i = 0; i < strlen(message); i++)
        {
            if (isalpha(message[i]))
            {
                message[i] = toupper(message[i]);
                message[i] = (key[j] - 65 + message[i] - 65) % 26 + 65;
                //  printf("%c , %c , %c\n",message[i],key[j],decoded[i]);
                j = j + 1;
                if (j == strlen(key))
                {
                    j = 0;
                }
                
            }
            else
            {
                message[i] = message[i];
            }
            
        }
    }

    void encode (char*message, char *key)
    {
        for(int i = 0; i < strlen(key);i++){
            key[i] = toupper(key[i]);
        }
        int j =0;
        for (int i = 0; i < strlen(message); i++)
        {
            if (isalpha(message[i]))
            {
                message[i] = toupper(message[i]);
                if (key[j] > message[i])
                {
                    message[i] = (26 - abs(message[i] - key[j])) + 65;
                }
                else
                {
                    message[i] = (message[i] - key[j]) % 26 + 65;
                }

                //printf("%c , %c , %c\n",decoded[i],key[j],encoded[i]);
                j = j + 1;
                if (j == strlen(key))
                {
                    j = 0;
                }
            }
            else
                message[i] = message[i];
        }
    }

    